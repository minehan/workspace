<?php
/**
 * 
 * ヘッダーの内容が記述されたテンプレートパーツです。
 *
 */
?>

  <!DOCTYPE html>
  <html <?php language_attributes(); ?>>

  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <title>
      <?php wp_title( '&laquo;', true, 'right'); ?>
        <?php bloginfo('name'); ?>
    </title>
    <link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/style.css" />
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <?php wp_head(); ?>
  </head>

  <body <?php body_class(); ?>>
    <div id="header">
<?php echo do_shortcode("[metaslider id=4]"); ?>

      <div id="pi">
        <p id="header-banner"><a href="<?php echo home_url(); ?>">ほーむ</a></p>
      </div>
      <div id="header-top" class="cfx">

        <div id="header-nav">

          <p id="header-telphone"></p>

          <p id="header-contact"><a href="<?php echo home_url(); ?>/contact/">お問い合わせ</a></p>

          <div id="header-menu">
            <?php wp_nav_menu( array('theme_location' => 'simplenav' , 'container' => false)); ?>

          </div>

        </div>

      </div>


    </div>