<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <title>example</title>
</head>
<body>
<?php
// コネクションを張る
$link = mysqli_connect("localhost", "sample", "1234", "sample");

// 文字コードの設定する
mysqli_set_charset($link, "utf8");

// INSERT文を作る
$sql = "INSERT INTO address (username, zip, address1, address2, tel)" . 
       "VALUES (" . 
       "'" . mysqli_real_escape_string($link, $_POST["username"]) . "'," . 
       "'" . mysqli_real_escape_string($link, $_POST["zip"]) . "'," . 
       "'" . mysqli_real_escape_string($link, $_POST["address1"]) . "'," . 
       "'" . mysqli_real_escape_string($link, $_POST["address2"]) . "'," . 
       "'" . mysqli_real_escape_string($link,$_POST["tel"]) . "');";

// INSERT文を発行する
mysqli_query($link, $sql);

// コネクションを閉じる
mysqli_close($link);
echo "レコードを追加しました";
?>
</body>
</html>