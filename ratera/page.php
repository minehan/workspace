<?php
/**
* page.phpは固定ページのデフォルトテンプレートです。
* 本テーマ「Sample Corporation」はカスタム固定ページテンプレート「page-top.php」を含むため、固定ページ編集画面から使用するテンプレートを切り替える機能が有効化されています。
* このテンプレートを使用したい時は「デフォルトテンプレート」を選択します（固定ページを新規作成時は初期値でデフォルトテンプレートが適用されます）。
*/
?>

<?php get_header(sub); ?>
  <div id="main" class="cfx">
    <div id="left-column">
      <?php if ( have_posts() ) : ?> <!--ループ開始　投稿があるなら-->
      <?php while ( have_posts() ) : the_post(); ?> <!--繰り返し処理開始-->
      <div <?php post_class(); ?>>
        <h1 class="page-title"><?php the_title(); ?></h1>
        <div class="page-content">
          <?php the_content(); ?>
        </div>
      </div>
      <?php endwhile; ?> <!--繰り返し終了-->
      <?php else : ?> <!--投稿がない場合の処理-->
        <h2>投稿が見つかりません</h2>
        <p><a href="<?php echo home_url(); ?>" >トップページに戻る</a></p>
      <?php endif; ?> <!--繰り返し終了-->
      </div>

    <div id="right-column">

<?php get_sidebar(); ?>

    </div>

  </div>

<?php get_footer(); 