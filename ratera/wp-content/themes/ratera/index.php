<?php
/**
* index.phpは最も優先順位が低く、かつ重要なページテンプレートです。
* 指定のテンプレートが見つからない時に、最終的に全てのページはこのindex.phpを利用します。
* 本学習用テーマ「Sample Corporation」では、全ての投稿アーカイブページで利用しています。
* アーカイブページのデザインを変更したい場合は、index.php複製してcategory.php/archive.phpなどを作成・変更するようにします。
*/
?>



<?php get_header(); ?>
<div id="main" class="cfx">
  <div id="left-column">
  <?php if ( have_posts() ) : ?> <!--ループ開始　投稿があるなら-->
  <?php while ( have_posts() ) : the_post(); ?><!--繰り返し処理開始-->
    <div class="archive-box">
      <div <?php post_class(); ?>>
         <h2 class="page-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
         <div class="page-meta">投稿日：<?php the_time('Y.m.j'); ?>　｜　カテゴリー：　<?php the_category(','); ?></div>
         <div class="page-content"><?php the_content(); ?></div>
      </div>
    </div>
  <?php endwhile; ?> <!--繰り返し処理終了-->
    <div id="post-link" class="cfx">
      <p id="post-link-prev"><?php next_posts_link( 'もっと見る' ); ?></p>
      <p id="post-link-next"><?php previous_posts_link( '新しい記事一覧へ戻る' ); ?></p>
    </div>
  <?php else : ?> <!--投稿がない場合-->
    <h2>投稿が見つかりません</h2>
    <p><a href="<?php echo home_url(); ?>">トップページに戻る</a></p>
  <?php endif; ?> <!--繰り返し終了-->
  </div>
  <div id="right-column">
  <?php get_sidebar(); ?>
  </div>
</div>
<?php get_footer();