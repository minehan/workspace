<?php
/**
* Template Name: トップページ用テンプレート
*
* これはカスタム固定ページテンプレートです。
* 固定ページ編集画面から使用するテンプレートを指定できます。
*/
?>

 <?php get_header(top); ?>
  <div id="main">
    <div id="top-mainmenu" class="cfx">
      <div id="top-new">
        <?php
        // ----------------新着情報　wp_query----------------------
        $args = array(
        'category_name' => 'new', //カテゴリー「new」を読み込む
        'posts_per_page' => 4 //表示数
        );
        $the_query = new WP_Query( $args ); //新規WP　queryを作成　変数argsで定義したパラメータを参照
        if ( $the_query->have_posts() ) : //ここから表示する内容を記入
        ?>
        <h2 id="top-new-title">新着情報</h2>
        <ul id="top-new-posts">
          <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?><!--ここから繰り返し-->
          <li class="top-new-post cfx">
            <a href="<?php the_permalink(); ?>">
              <?php if (has_post_thumbnail()) { the_post_thumbnail( array(120,120) );
              } else { ?>
              <img src="<?php echo get_template_directory_uri(); ?>/img/noimage.png" width="120" height="120" />
              <?php } ?>
              <span><?php the_time('Y.m.j') ?></span>
              <h3><?php the_title(); ?></h3>
            </a>
          </li>
          <?php endwhile; ?> <!--ここまで繰り返し-->
        </ul>
        <p id="top-new-link"><a href="<?php echo home_url(); ?>/category/new/"><i class="fa fa-angle-right"></i>&nbsp;一覧を見る</a></p>
        <?php
        endif; wp_reset_postdata();
        // ----------------新着情報WP_query終了----------------------
        ?>

      </div>
      <div id="top-shop">
        <h2 id="top-shop-title">店舗情報</h2>
        <p class="top-shop-map"><img src="<?php echo get_template_directory_uri(); ?>/img/map.png" width ="380" height="245" alt="map"></p>
        <table class="top-shop-information">
          <tr class="top-shop-frame"><th class="top-shop-headline">営業時間</th><td class="top-shop-content">&nbsp;１１：３０〜１４：００（LO）<br>&nbsp;１７：３０〜２３：００（LO）</td></tr>
          <tr class="top-shop-frame"><th class="top-shop-headline">定休日</th><td class="top-shop-content">第２、４火曜日</td></tr>
          <tr class="top-shop-frame"><th class="top-shop-headline">TEL</th><td class="top-shop-content">０００ー００００−００００</td></tr>
          <tr class="top-shop-frame"><th class="top-shop-headline">住所</th><td class="top-shop-content">埼玉県川口市中青木３丁目１０番４３号プラムコーポ６−１</td></tr>
        </table>

        <p id="top-shop-link"><a href="<?php echo home_url(); ?>/acces/"><i class="fa fa-angle-right"></i>&nbsp;詳しく見る</a></p>
      </div>
    </div>

    <div id="top-menu">
      <h2 id="top-menu-title">トップメニュー</h2>
      <div id="top-menu-item01">
        <p class="top-menu-item-title">ホロホロ鳥のスープ</p>
        <p class="top-menu-item-content">フレンチなどでよく用いられるホロホロ鳥を野菜の出汁で優しい味に仕上げました。</p>
        <p class="top-menu-item-price">&yen;400</p>
      </div>
      <div id="top-menu-item02">
        <p class="top-menu-item-title">シーフードサラダ</p>
        <p class="top-menu-item-content">才巻海老やホタテの貝柱をふんだんに使用し、西洋ワサビベースドレッシングで大人な味付けに。</p>
        <p class="top-menu-item-price">&yen;650</p>
      </div>
      <div id="top-menu-item03">
        <p class="top-menu-item-title">中華風ホルモンパスタ</p>
        <p class="top-menu-item-content">ら・てらの創作パスタ、トリッパ、レバー、砂肝などのホルモンを自家製のオイスターソースで。</p>
        <p class="top-menu-item-price">&yen;600</p>
      </div>
      <div id="top-menu-item04">
        <p class="top-menu-item-title">旬菜のペペロンチーノ</p>
        <p class="top-menu-item-content">春夏秋冬、その時期に一番美味しい野菜をペペロンチーノの具材に。</p>
        <p class="top-menu-item-price">&yen;650</p>
      </div>
      <div id="top-menu-item05">
        <p class="top-menu-item-title">ら・てらバーガー</p>
        <p class="top-menu-item-content">鹿や猪などジビエ肉で作ったパティを使用した一品、ジビエ料理特有の野趣をお楽しみ下さい。</p>
        <p class="top-menu-item-price">&yen;550</p>
      </div>
      <div id="top-menu-item06">
        <p class="top-menu-item-title">チーズドッグ</p>
        <p class="top-menu-item-content">チーズの王様パルミジャーノ・レッジャーノを使用した気品溢れる一品。</p>
        <p class="top-menu-item-price">&yen;450</p>
      </div>
      <div id="top-menu-item07">
        <p class="top-menu-item-title">パウンドケーキ</p>
        <p class="top-menu-item-content">自家製のパウンドケーキをキャラメル仕立てに。</p>
        <p class="top-menu-item-price">&yen;480</p>
      </div>
      <div id="top-menu-item08">
        <p class="top-menu-item-title">フルーツパフェ</p>
        <p class="top-menu-item-content">まるで宝石箱、色々なフルーツがこの一皿で楽しめます。</p>
        <p class="top-menu-item-price">&yen;580</p>
      </div>

      <p id="top-menu-link"><a href="<?php echo home_url(); ?>/menu/"><i class="fa fa-angle-right"></i>&nbsp;他のメニューを見る</a></p>
    </div>
</div>

<?php get_footer();
