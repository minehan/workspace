<?php
/**
 *
 * サイドバーの内容が記述されたテンプレートパーツです。
 * 上部にオリジナルデザインのサイドバーがあります。
 * 下部には管理画面から変更可能なウィジェットエリアを作成します(dynamic_sidebarの行)。
 * ウィジェットエリアに表示する内容は管理画面「外観＞ウィジェット」から管理します。
 *
 */
?>

<div id="sidebar-main">

  <div id="sidebar-menu">

    <h3 id="sidebar-menu-title">MENU</h3>

    <ul id="sidebar-menu-parent">
      <li><a href="<?php echo home_url(); ?>">ほーむ</a></li>
      <li><a href="<?php echo home_url(); ?>/new/">にゅーす</a></li>
      <li><a href="<?php echo home_url(); ?>/menu/">めにゅー</a></li>
      <li><a href="<?php echo home_url(); ?>/acces/">あくせす</a></li>
      <li><a href="<?php echo home_url(); ?>/concept/">こんせぷと</a></li>
    </ul>

  </div>

  <div id="sidebar-contact">
    <p id="sidebar-contact-tel"><img src="<?php echo get_template_directory_uri(); ?>/img/sidebar-tel.png" alt="お電話はこちら" width="220" height="110" /></p>
  </div>

</div>
<?php dynamic_sidebar( 'サイドバー' ); ?>