<?php
/**
* 
* フッターの内容が記述されたテンプレートパーツです。
*
*/
?>

  <div id="footer">

    <div id="footer-wrapper">
      <p id="footer-logo"><img src="<?php echo get_template_directory_uri(); ?>/img/footerlogo.png" alt=""></p>
      <table class="top-footer-information">
        <tr class="top-footer-frame">
          <th class="top-footer-headline">会社名</th>
          <td class="top-footer-content">ら・てら&nbsp;inc.</td>
        </tr>
        <tr class="top-footer-frame">
          <th class="top-footer-headline">所在地</th>
          <td class="top-footer-content">埼玉県川口市中青木３丁目１０番４３号</td>
        </tr>
        <tr class="top-footer-frame">
          <th class="top-footer-headline">設立</th>
          <td class="top-footer-content">２０１６年３月</td>
        </tr>
        <tr class="top-footer-frame">
          <th class="top-footer-headline">役員</th>
          <td class="top-footer-content">代表取締役：藤本卓也</td>
        </tr>
      </table>
    </div>

  </div>

  <div id="footer-copyright">
    <p>copyright&nbsp;(C)&nbsp;2016
      <?php bloginfo( 'name' ); ?> All Right Reserved.</p>
  </div>

  <?php wp_footer(); ?>

    </body>

    </html>