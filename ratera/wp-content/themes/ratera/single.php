<?php
/**
* single.phpは投稿ページテンプレートです。
* このテンプレートを利用する全てのページでコメント機能を無効化したい場合は　comments_template();　の行を削除します。
* （投稿ごとのコメント有効/無効化は、投稿編集画面の「ディスカッション」で切り替えます）
*/
?>

<?php get_header(sub); ?>
 
  <div id="main" class="cfx">
    <div id="left-column">
      <?php if ( have_posts() ) : ?> <!--ループ開始　投稿があるなら-->
      <?php while ( have_posts() ) : the_post(); ?> <!--繰り返し処理開始-->
        <div <?php post_class(); ?>>
          <h1 class="page-title"><?php the_title(); ?></h1>
          <div class="page-meta">投稿日：<?php the_time('Y.m.j'); ?>　｜　カテゴリー：　<?php the_category(','); ?>
          </div>
          <div class="page-content"><?php the_content(); ?>
          </div>
        </div>
        <div id="post-link" class="cfx">
          <p id="post-link-prev"><?php previous_post_link( '%link', '前の記事へ' ); ?></p>
          <p id="post-link-next"><?php next_post_link( '%link', '新しい記事へ' ); ?></p>
        </div>
        <div id="commnet-container"><?php comments_template(); ?></div>
      <?php endwhile; ?> <!--繰り返し終了-->
      <?php else : ?> <!--投稿がない場合-->
        <h2>投稿が見つかりません。</h2>
        <p><a href="<?php echo home_url(); ?>">トップページに戻る</a></p>
      <?php endif; ?> <!--繰り返し終了-->
    </div>

    <div id="right-column">

<?php get_sidebar(); ?>

    </div>

  </div>

<?php get_footer();