<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * TechniqueSkillLanguage Controller
 *
 * @property \App\Model\Table\TechniqueSkillLanguageTable $TechniqueSkillLanguage
 */
class TechniqueSkillLanguageController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
  public $paginate = [
    'limit' => 10,
    'order' => [
      'techniqueSkillLanguage.name' => 'asc'
    ]
  ];

    public function index()
    {
        $this->set('title', 'Nsystem｜プログラミングスキル');
        $techniqueSkillLanguage = $this->paginate($this->TechniqueSkillLanguage);

        $this->set(compact('techniqueSkillLanguage'));
        $this->set('_serialize', ['techniqueSkillLanguage']);
    }

    /**
     * View method
     *
     * @param string|null $id Technique Skill Language id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->set('title', 'Nsystem｜閲覧');
        $techniqueSkillLanguage = $this->TechniqueSkillLanguage->get($id, [
            'contain' => []
        ]);

        $this->set('techniqueSkillLanguage', $techniqueSkillLanguage);
        $this->set('_serialize', ['techniqueSkillLanguage']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->set('title', 'Nsystem｜新規登録');
        $techniqueSkillLanguage = $this->TechniqueSkillLanguage->newEntity();
        if ($this->request->is('post')) {
            $techniqueSkillLanguage = $this->TechniqueSkillLanguage->patchEntity($techniqueSkillLanguage, $this->request->data);
            if ($this->TechniqueSkillLanguage->save($techniqueSkillLanguage)) {
                $this->Flash->success(__('新規登録が無事に成功しました'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('失敗しました、もう一度やり直してください'));
            }
        }
        $this->set(compact('techniqueSkillLanguage'));
        $this->set('_serialize', ['techniqueSkillLanguage']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Technique Skill Language id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->set('title', 'Nsystem｜編集');
        $techniqueSkillLanguage = $this->TechniqueSkillLanguage->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $techniqueSkillLanguage = $this->TechniqueSkillLanguage->patchEntity($techniqueSkillLanguage, $this->request->data);
            if ($this->TechniqueSkillLanguage->save($techniqueSkillLanguage)) {
                $this->Flash->success(__('編集が無事に成功しました'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('失敗しました、もう一度やり直してください'));
            }
        }
        $this->set(compact('techniqueSkillLanguage'));
        $this->set('_serialize', ['techniqueSkillLanguage']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Technique Skill Language id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $techniqueSkillLanguage = $this->TechniqueSkillLanguage->get($id);
        if ($this->TechniqueSkillLanguage->delete($techniqueSkillLanguage)) {
            $this->Flash->success(__('削除が無事に成功しました'));
        } else {
            $this->Flash->error(__('失敗しました、もう一度やり直してください'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
