<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * TechniqueSkillDb Controller
 *
 * @property \App\Model\Table\TechniqueSkillDbTable $TechniqueSkillDb
 */
class TechniqueSkillDbController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
  public $paginate = [
    'limit' => 10,
    'order' => [
      'techniqueSkillDb.name' => 'asc'
    ]
  ];

    public function index()
    {
        $this->set('title', 'Nsystem｜データベーススキル');
        $techniqueSkillDb = $this->paginate($this->TechniqueSkillDb);

        $this->set(compact('techniqueSkillDb'));
        $this->set('_serialize', ['techniqueSkillDb']);
    }

    /**
     * View method
     *
     * @param string|null $id Technique Skill Db id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->set('title', 'Nsystem｜閲覧');
        $techniqueSkillDb = $this->TechniqueSkillDb->get($id, [
            'contain' => []
        ]);

        $this->set('techniqueSkillDb', $techniqueSkillDb);
        $this->set('_serialize', ['techniqueSkillDb']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->set('title', 'Nsystem｜新規登録');
        $techniqueSkillDb = $this->TechniqueSkillDb->newEntity();
        if ($this->request->is('post')) {
            $techniqueSkillDb = $this->TechniqueSkillDb->patchEntity($techniqueSkillDb, $this->request->data);
            if ($this->TechniqueSkillDb->save($techniqueSkillDb)) {
                $this->Flash->success(__('新規登録が無事に成功しました'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('失敗しました、もう一度やり直してください'));
            }
        }
        $this->set(compact('techniqueSkillDb'));
        $this->set('_serialize', ['techniqueSkillDb']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Technique Skill Db id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->set('title', 'Nsystem｜編集');
        $techniqueSkillDb = $this->TechniqueSkillDb->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $techniqueSkillDb = $this->TechniqueSkillDb->patchEntity($techniqueSkillDb, $this->request->data);
            if ($this->TechniqueSkillDb->save($techniqueSkillDb)) {
                $this->Flash->success(__('編集が無事に成功しました'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('失敗しました、もう一度やり直してください'));
            }
        }
        $this->set(compact('techniqueSkillDb'));
        $this->set('_serialize', ['techniqueSkillDb']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Technique Skill Db id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $techniqueSkillDb = $this->TechniqueSkillDb->get($id);
        if ($this->TechniqueSkillDb->delete($techniqueSkillDb)) {
            $this->Flash->success(__('削除が無事に成功しました'));
        } else {
            $this->Flash->error(__('失敗しました、もう一度やり直してください'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
