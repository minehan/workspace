<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * SalesPerformance Controller
 *
 * @property \App\Model\Table\SalesPerformanceTable $SalesPerformance
 */
class SalesPerformanceController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
  public $paginate = [
    'limit' => 10,
    'order' => [
      'salesPerformance.name' => 'asc'
    ]
  ];
  
    public function index()
    {
        $this->set('title', 'Nsystem｜営業実績');
        $salesPerformance = $this->paginate($this->SalesPerformance);

        $this->set(compact('salesPerformance'));
        $this->set('_serialize', ['salesPerformance']);
    }

    /**
     * View method
     *
     * @param string|null $id Sales Performance id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->set('title', 'Nsystem｜閲覧');
        $salesPerformance = $this->SalesPerformance->get($id, [
            'contain' => []
        ]);

        $this->set('salesPerformance', $salesPerformance);
        $this->set('_serialize', ['salesPerformance']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->set('title', 'Nsystem｜新規登録');
        $salesPerformance = $this->SalesPerformance->newEntity();
        if ($this->request->is('post')) {
            $salesPerformance = $this->SalesPerformance->patchEntity($salesPerformance, $this->request->data);
            if ($this->SalesPerformance->save($salesPerformance)) {
                $this->Flash->success(__('新規登録が無事に成功しました'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('失敗しました、もう一度やり直してください'));
            }
        }
        $this->set(compact('salesPerformance'));
        $this->set('_serialize', ['salesPerformance']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Sales Performance id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->set('title', 'Nsystem｜編集');
        $salesPerformance = $this->SalesPerformance->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $salesPerformance = $this->SalesPerformance->patchEntity($salesPerformance, $this->request->data);
            if ($this->SalesPerformance->save($salesPerformance)) {
                $this->Flash->success(__('編集が無事に成功しました'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('失敗しました、もう一度やり直してください'));
            }
        }
        $this->set(compact('salesPerformance'));
        $this->set('_serialize', ['salesPerformance']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Sales Performance id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $salesPerformance = $this->SalesPerformance->get($id);
        if ($this->SalesPerformance->delete($salesPerformance)) {
            $this->Flash->success(__('削除が無事に成功しました'));
        } else {
            $this->Flash->error(__('失敗しました、もう一度やり直してください'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
