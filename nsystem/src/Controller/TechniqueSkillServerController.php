<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * TechniqueSkillServer Controller
 *
 * @property \App\Model\Table\TechniqueSkillServerTable $TechniqueSkillServer
 */
class TechniqueSkillServerController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
  public $paginate = [
    'limit' => 10,
    'order' => [
      'techniqueSkillServer.name' => 'asc'
    ]
  ];

    public function index()
    {
        $this->set('title', 'Nsystem｜サーバースキル');
        $techniqueSkillServer = $this->paginate($this->TechniqueSkillServer);

        $this->set(compact('techniqueSkillServer'));
        $this->set('_serialize', ['techniqueSkillServer']);
    }

    /**
     * View method
     *
     * @param string|null $id Technique Skill Server id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->set('title', 'Nsystem｜閲覧');
        $techniqueSkillServer = $this->TechniqueSkillServer->get($id, [
            'contain' => []
        ]);

        $this->set('techniqueSkillServer', $techniqueSkillServer);
        $this->set('_serialize', ['techniqueSkillServer']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->set('title', 'Nsystem｜新規登録');
        $techniqueSkillServer = $this->TechniqueSkillServer->newEntity();
        if ($this->request->is('post')) {
            $techniqueSkillServer = $this->TechniqueSkillServer->patchEntity($techniqueSkillServer, $this->request->data);
            if ($this->TechniqueSkillServer->save($techniqueSkillServer)) {
                $this->Flash->success(__('新規登録が無事に成功しました'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('失敗しました、もう一度やり直してください'));
            }
        }
        $this->set(compact('techniqueSkillServer'));
        $this->set('_serialize', ['techniqueSkillServer']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Technique Skill Server id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->set('title', 'Nsystem｜編集');
        $techniqueSkillServer = $this->TechniqueSkillServer->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $techniqueSkillServer = $this->TechniqueSkillServer->patchEntity($techniqueSkillServer, $this->request->data);
            if ($this->TechniqueSkillServer->save($techniqueSkillServer)) {
                $this->Flash->success(__('編集が無事に成功しました'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('失敗しました、もう一度やり直してください'));
            }
        }
        $this->set(compact('techniqueSkillServer'));
        $this->set('_serialize', ['techniqueSkillServer']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Technique Skill Server id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $techniqueSkillServer = $this->TechniqueSkillServer->get($id);
        if ($this->TechniqueSkillServer->delete($techniqueSkillServer)) {
            $this->Flash->success(__('削除が無事に成功しました'));
        } else {
            $this->Flash->error(__('失敗しました、もう一度やり直してください'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
