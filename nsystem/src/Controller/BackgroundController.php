<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Background Controller
 *
 * @property \App\Model\Table\BackgroundTable $Background
 */
class BackgroundController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
  public $paginate = [
    'limit' => 10,
    'order' => [
      'background.name' => 'asc'
    ]
  ];
  
    public function index()
    {
        $this->set('title', 'Nsystem｜経歴');
        $background = $this->paginate($this->Background);

        $this->set(compact('background'));
        $this->set('_serialize', ['background']);
    }

    /**
     * View method
     *
     * @param string|null $id Background id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->set('title', 'Nsystem｜閲覧');
        $background = $this->Background->get($id, [
            'contain' => []
        ]);

        $this->set('background', $background);
        $this->set('_serialize', ['background']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->set('title', 'Nsystem｜新規登録');
        $background = $this->Background->newEntity();
        if ($this->request->is('post')) {
            $background = $this->Background->patchEntity($background, $this->request->data);
            if ($this->Background->save($background)) {
                $this->Flash->success(__('新規登録が無事に成功しました'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('失敗しました、もう一度やり直してください'));
            }
        }
        $this->set(compact('background'));
        $this->set('_serialize', ['background']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Background id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->set('title', 'Nsystem｜編集');
        $background = $this->Background->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $background = $this->Background->patchEntity($background, $this->request->data);
            if ($this->Background->save($background)) {
                $this->Flash->success(__('編集が無事に成功しました'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('失敗しました、もう一度やり直してください'));
            }
        }
        $this->set(compact('background'));
        $this->set('_serialize', ['background']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Background id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $background = $this->Background->get($id);
        if ($this->Background->delete($background)) {
            $this->Flash->success(__('削除が無事に成功しました'));
        } else {
            $this->Flash->error(__('失敗しました、もう一度やり直してください'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
