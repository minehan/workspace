<?php
namespace App\Controller;

use App\Controller\AppController;
//他のテーブルを使用
use Cake\ORM\TableRegistry;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{

  /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
  public $paginate = [
    'limit' => 10,
    'order' => [
      'user.name' => 'asc'
    ]
  ];

  public function initialize(){
    parent::initialize();
    //他のテーブルを使用
    $this->Personal = TableRegistry::get('personal');
    $this->Background = TableRegistry::get('background');
    $this->SalesPerformance = TableRegistry::get('salesPerformance');
    $this->SalesSkill = TableRegistry::get('salesSkill');
    $this->TechniquePerformance = TableRegistry::get('techniquePerformance');
    $this->TechniqueSkill = TableRegistry::get('techniqueSkill');
    $this->TechniqueSkillDb = TableRegistry::get('techniqueSkillDb');
    $this->TechniqueSkillDesign = TableRegistry::get('techniqueSkillDesign');
    $this->TechniqueSkillFramework = TableRegistry::get('techniqueSkillFramework');
    $this->TechniqueSkillLanguage = TableRegistry::get('techniqueSkillLanguage');
    $this->TechniqueSkillNetwork = TableRegistry::get('techniqueSkillNetwork');
    $this->TechniqueSkillServer = TableRegistry::get('techniqueSkillServer');
  }
  
  public function index(){
    $this->set('title', 'Nsystem');
    $this->viewBuilder()->layout('index');
  }

  public function home()
  {
    $this->set('title', 'Nsystem｜トップページ');

    //空の配列を用意してそこに検索データを入れる 
    $personal = [];
    $background = [];
    $salesPerformance = [];
    $salesSkill = [];
    $techniquePerformance = [];
    $techniqueSkill = [];
    $techniqueSkillDb = [];
    $techniqueSkillDesign = [];
    $techniqueSkillFramework = [];
    $techniqueSkillLanguage = [];
    $techniqueSkillNetwork = [];
    $techniqueSkillServer = [];
    $personalCount = [];
    $backgroundCount = [];
    $salesSkillCount = [];
    $salesPerformanceCount = [];
    $techniquePerformanceCount = [];
    $techniqueSkillCount = [];
    $techniqueSkillDbCount = [];
    $techniqueSkillDesignCount = [];
    $techniqueSkillFrameworkCount = [];
    $techniqueSkillLanguageCount = [];
    $techniqueSkillNetworkCount = [];
    $techniqueSkillServerCount = [];

    if ($this->request->is('post')) {
      $find = $this->request->data['find'];
      //該当データの取得
      $personal = $this->Personal->find()->where(["number" => $find ]);
      $background = $this->Background->find()->where(["number" => $find ]);
      $salesPerformance = $this->SalesPerformance->find()->where(["number" => $find ]);
      $salesSkill = $this->SalesSkill->find()->where(["number" => $find ]);
      $techniquePerformance = $this->TechniquePerformance->find()->where(["number" => $find ]);
      $techniqueSkill = $this->TechniqueSkill->find()->where(["number" => $find ]);
      $techniqueSkillDb = $this->TechniqueSkillDb->find()->where(["number" => $find ]);
      $techniqueSkillDesign = $this->TechniqueSkillDesign->find()->where(["number" => $find ]);
      $techniqueSkillFramework = $this->TechniqueSkillFramework->find()->where(["number" => $find ]);
      $techniqueSkillLanguage = $this->TechniqueSkillLanguage->find()->where(["number" => $find ]);
      $techniqueSkillNetwork = $this->TechniqueSkillNetwork->find()->where(["number" => $find ]);
      $techniqueSkillServer = $this->TechniqueSkillServer->find()->where(["number" => $find ]);
      //該当件数の取得
      $personalCount = $this->Personal->find()->where(["number" => $find ])->count();
      $backgroundCount = $this->Background->find()->where(["number" => $find ])->count();
      $salesPerformanceCount = $this->SalesPerformance->find()->where(["number" => $find ])->count();
      $salesSkillCount = $this->SalesSkill->find()->where(["number" => $find ])->count();
      $techniquePerformanceCount = $this->TechniquePerformance->find()->where(["number" => $find ])->count();
      $techniqueSkillCount = $this->TechniqueSkill->find()->where(["number" => $find ])->count();
      $techniqueSkillDbCount = $this->TechniqueSkillDb->find()->where(["number" => $find ])->count();
      $techniqueSkillDesignCount = $this->TechniqueSkillDesign->find()->where(["number" => $find ])->count();
      $techniqueSkillFrameworkCount = $this->TechniqueSkillFramework->find()->where(["number" => $find ])->count();
      $techniqueSkillLanguageCount = $this->TechniqueSkillLanguage->find()->where(["number" => $find ])->count();
      $techniqueSkillNetworkCount = $this->TechniqueSkillNetwork->find()->where(["number" => $find ])->count();
      $techniqueSkillServerCount = $this->TechniqueSkillServer->find()->where(["number" => $find ])->count();
      $count = $personalCount + $backgroundCount + $salesPerformanceCount + $salesSkillCount + $techniquePerformanceCount + $techniqueSkillCount + $techniqueSkillDbCount + $techniqueSkillDesignCount + $techniqueSkillFrameworkCount + $techniqueSkillLanguageCount + $techniqueSkillNetworkCount + $techniqueSkillServerCount;
      if($count === 0){
        $this->Flash->error(__('該当のデータがありません'));
      }else{
        $this->Flash->success(__('社員番号'. $find . 'は下記で'. $count . '件登録されています'));
      }
    }

    //setしないとviewで表示できない
    $this->set('personal', $personal);
    $this->set('background', $background);
    $this->set('salesPerformance', $salesPerformance);
    $this->set('salesSkill', $salesSkill);
    $this->set('techniquePerformance', $techniquePerformance);
    $this->set('techniqueSkill', $techniqueSkill);
    $this->set('techniqueSkillDb', $techniqueSkillDb);
    $this->set('techniqueSkillDesign', $techniqueSkillDesign);
    $this->set('techniqueSkillFramework', $techniqueSkillFramework);
    $this->set('techniqueSkillLanguage', $techniqueSkillLanguage);
    $this->set('techniqueSkillNetwork', $techniqueSkillNetwork);
    $this->set('techniqueSkillServer', $techniqueSkillServer);
    $this->set('personalCount', $personalCount);
    $this->set('backgroundCount', $backgroundCount);
    $this->set('salesPerformanceCount', $salesPerformanceCount);
    $this->set('salesSkillCount', $salesSkillCount);
    $this->set('techniquePerformanceCount', $techniquePerformanceCount);
    $this->set('techniqueSkillCount', $techniqueSkillCount);
    $this->set('techniqueSkillDbCount', $techniqueSkillDbCount);
    $this->set('techniqueSkillDesignCount', $techniqueSkillDesignCount);
    $this->set('techniqueSkillFrameworkCount', $techniqueSkillFrameworkCount);
    $this->set('techniqueSkillLanguageCount', $techniqueSkillLanguageCount);
    $this->set('techniqueSkillNetworkCount', $techniqueSkillNetworkCount);
    $this->set('techniqueSkillServerCount', $techniqueSkillServerCount);
  }

  public function secret()
  {
    $this->set('title', 'Nsystem｜管理者ページ');
    $users = $this->paginate($this->Users);

    $this->set(compact('users'));
    $this->set('_serialize', ['users']);
  }

  /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
  public function view($id = null)
  {
    $user = $this->Users->get($id, [
      'contain' => []
    ]);

    $this->set('user', $user);
    $this->set('_serialize', ['user']);
  }

  /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
  public function add()
  {
    $this->set('title', 'Nsystem｜新規登録');
    $user = $this->Users->newEntity();
    if ($this->request->is('post')) {
      $user = $this->Users->patchEntity($user, $this->request->data);
      if ($this->Users->save($user)) {
        $this->Flash->success(__('新規登録が無事に成功しました'));

        return $this->redirect(['action' => 'secret']);
      } else {
        $this->Flash->error(__('失敗しました、もう一度やり直してください'));
      }
    }
    $this->set(compact('user'));
    $this->set('_serialize', ['user']);
  }

  /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
  public function edit($id = null)
  {
    $this->set('title', 'Nsystem｜編集');
    $user = $this->Users->get($id, [
      'contain' => []
    ]);
    if ($this->request->is(['patch', 'post', 'put'])) {
      $user = $this->Users->patchEntity($user, $this->request->data);
      if ($this->Users->save($user)) {
        $this->Flash->success(__('編集が無事に成功しました'));

        return $this->redirect(['action' => 'secret']);
      } else {
        $this->Flash->error(__('失敗しました、もう一度やり直してください'));
      }
    }
    $this->set(compact('user'));
    $this->set('_serialize', ['user']);
  }

  /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
  public function delete($id = null)
  {
    $this->request->allowMethod(['post', 'delete']);
    $user = $this->Users->get($id);
    if ($this->Users->delete($user)) {
      $this->Flash->success(__('削除が無事に成功しました'));
    } else {
      $this->Flash->error(__('失敗しました、もう一度やり直してください'));
    }

    return $this->redirect(['action' => 'secret']);
  }
  public function login()
  {
    $this->set('title', 'Nsytem｜ログイン');
    if($this->request->is('post')) {
      $user = $this->Auth->identify();

      if($user){
        $this->Auth->setUser($user);
        return $this->redirect($this->Auth->redirectUrl());
      }
      $this->Flash->error('ログインエラーです');
    }
  }
  public function logout()
  {
    return $this->redirect($this->Auth->logout());
  }
  public function beforeFilter(\Cake\Event\Event $event)
  {
    $this->Auth->allow(['add']);
    $this->Auth->allow(['index']);
  }
}
