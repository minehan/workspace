<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * SalesSkill Controller
 *
 * @property \App\Model\Table\SalesSkillTable $SalesSkill
 */
class SalesSkillController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
  public $paginate = [
    'limit' => 10,
    'order' => [
      'salesSkill.name' => 'asc'
    ]
  ];

    public function index()
    {
        $this->set('title', 'Nsystem｜営業資格');
        $salesSkill = $this->paginate($this->SalesSkill);

        $this->set(compact('salesSkill'));
        $this->set('_serialize', ['salesSkill']);
    }

    /**
     * View method
     *
     * @param string|null $id Sales Skill id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->set('title', 'Nsystem｜閲覧');
        $salesSkill = $this->SalesSkill->get($id, [
            'contain' => []
        ]);

        $this->set('salesSkill', $salesSkill);
        $this->set('_serialize', ['salesSkill']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->set('title', 'Nsystem｜新規登録');
        $salesSkill = $this->SalesSkill->newEntity();
        if ($this->request->is('post')) {
            $salesSkill = $this->SalesSkill->patchEntity($salesSkill, $this->request->data);
            if ($this->SalesSkill->save($salesSkill)) {
                $this->Flash->success(__('新規登録が無事に成功しました'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('失敗しました、もう一度やり直してください'));
            }
        }
        $this->set(compact('salesSkill'));
        $this->set('_serialize', ['salesSkill']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Sales Skill id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->set('title', 'Nsystem｜編集');
        $salesSkill = $this->SalesSkill->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $salesSkill = $this->SalesSkill->patchEntity($salesSkill, $this->request->data);
            if ($this->SalesSkill->save($salesSkill)) {
                $this->Flash->success(__('編集が無事に成功しました'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('失敗しました、もう一度やり直してください'));
            }
        }
        $this->set(compact('salesSkill'));
        $this->set('_serialize', ['salesSkill']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Sales Skill id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $salesSkill = $this->SalesSkill->get($id);
        if ($this->SalesSkill->delete($salesSkill)) {
            $this->Flash->success(__('削除が無事に成功しました'));
        } else {
            $this->Flash->error(__('失敗しました、もう一度やり直してください'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
