<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * TechniqueSkillFramework Controller
 *
 * @property \App\Model\Table\TechniqueSkillFrameworkTable $TechniqueSkillFramework
 */
class TechniqueSkillFrameworkController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
  public $paginate = [
    'limit' => 10,
    'order' => [
      'techniqueSkillFramework.name' => 'asc'
    ]
  ];

    public function index()
    {
        $this->set('title', 'Nsystem｜フレームワークスキル');
        $techniqueSkillFramework = $this->paginate($this->TechniqueSkillFramework);

        $this->set(compact('techniqueSkillFramework'));
        $this->set('_serialize', ['techniqueSkillFramework']);
    }

    /**
     * View method
     *
     * @param string|null $id Technique Skill Framework id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->set('title', 'Nsystem｜閲覧');
        $techniqueSkillFramework = $this->TechniqueSkillFramework->get($id, [
            'contain' => []
        ]);

        $this->set('techniqueSkillFramework', $techniqueSkillFramework);
        $this->set('_serialize', ['techniqueSkillFramework']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->set('title', 'Nsystem｜新規登録');
        $techniqueSkillFramework = $this->TechniqueSkillFramework->newEntity();
        if ($this->request->is('post')) {
            $techniqueSkillFramework = $this->TechniqueSkillFramework->patchEntity($techniqueSkillFramework, $this->request->data);
            if ($this->TechniqueSkillFramework->save($techniqueSkillFramework)) {
                $this->Flash->success(__('新規登録が無事に成功しました'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('失敗しました、もう一度やり直してください'));
            }
        }
        $this->set(compact('techniqueSkillFramework'));
        $this->set('_serialize', ['techniqueSkillFramework']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Technique Skill Framework id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->set('title', 'Nsystem｜編集');
        $techniqueSkillFramework = $this->TechniqueSkillFramework->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $techniqueSkillFramework = $this->TechniqueSkillFramework->patchEntity($techniqueSkillFramework, $this->request->data);
            if ($this->TechniqueSkillFramework->save($techniqueSkillFramework)) {
                $this->Flash->success(__('編集が無事に成功しました'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('失敗しました、もう一度やり直してください'));
            }
        }
        $this->set(compact('techniqueSkillFramework'));
        $this->set('_serialize', ['techniqueSkillFramework']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Technique Skill Framework id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $techniqueSkillFramework = $this->TechniqueSkillFramework->get($id);
        if ($this->TechniqueSkillFramework->delete($techniqueSkillFramework)) {
            $this->Flash->success(__('削除が無事に成功しました'));
        } else {
            $this->Flash->error(__('失敗しました、もう一度やり直してください'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
