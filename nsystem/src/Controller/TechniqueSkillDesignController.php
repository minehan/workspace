<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * TechniqueSkillDesign Controller
 *
 * @property \App\Model\Table\TechniqueSkillDesignTable $TechniqueSkillDesign
 */
class TechniqueSkillDesignController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
  public $paginate = [
    'limit' => 10,
    'order' => [
      'techniqueSkillDesign.name' => 'asc'
    ]
  ];

    public function index()
    {
        $this->set('title', 'Nsystem｜デザインスキル');
        $techniqueSkillDesign = $this->paginate($this->TechniqueSkillDesign);

        $this->set(compact('techniqueSkillDesign'));
        $this->set('_serialize', ['techniqueSkillDesign']);
    }

    /**
     * View method
     *
     * @param string|null $id Technique Skill Design id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->set('title', 'Nsystem｜閲覧');
        $techniqueSkillDesign = $this->TechniqueSkillDesign->get($id, [
            'contain' => []
        ]);

        $this->set('techniqueSkillDesign', $techniqueSkillDesign);
        $this->set('_serialize', ['techniqueSkillDesign']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->set('title', 'Nsystem｜新規登録');
        $techniqueSkillDesign = $this->TechniqueSkillDesign->newEntity();
        if ($this->request->is('post')) {
            $techniqueSkillDesign = $this->TechniqueSkillDesign->patchEntity($techniqueSkillDesign, $this->request->data);
            if ($this->TechniqueSkillDesign->save($techniqueSkillDesign)) {
                $this->Flash->success(__('新規登録が無事に成功しました'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('失敗しました、もう一度やり直してください'));
            }
        }
        $this->set(compact('techniqueSkillDesign'));
        $this->set('_serialize', ['techniqueSkillDesign']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Technique Skill Design id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->set('title', 'Nsystem｜編集');
        $techniqueSkillDesign = $this->TechniqueSkillDesign->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $techniqueSkillDesign = $this->TechniqueSkillDesign->patchEntity($techniqueSkillDesign, $this->request->data);
            if ($this->TechniqueSkillDesign->save($techniqueSkillDesign)) {
                $this->Flash->success(__('編集が無事に成功しました'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('失敗しました、もう一度やり直してください'));
            }
        }
        $this->set(compact('techniqueSkillDesign'));
        $this->set('_serialize', ['techniqueSkillDesign']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Technique Skill Design id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $techniqueSkillDesign = $this->TechniqueSkillDesign->get($id);
        if ($this->TechniqueSkillDesign->delete($techniqueSkillDesign)) {
            $this->Flash->success(__('削除が無事に成功しました'));
        } else {
            $this->Flash->error(__('失敗しました、もう一度やり直してください'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
