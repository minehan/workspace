<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * TechniqueSkill Controller
 *
 * @property \App\Model\Table\TechniqueSkillTable $TechniqueSkill
 */
class TechniqueSkillController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
  public $paginate = [
    'limit' => 10,
    'order' => [
      'techniqueSkill.name' => 'asc'
    ]
  ];

    public function index()
    {
        $this->set('title', 'Nsystem｜技術資格');
        $techniqueSkill = $this->paginate($this->TechniqueSkill);

        $this->set(compact('techniqueSkill'));
        $this->set('_serialize', ['techniqueSkill']);
    }

    /**
     * View method
     *
     * @param string|null $id Technique Skill id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->set('title', 'Nsystem｜閲覧');
        $techniqueSkill = $this->TechniqueSkill->get($id, [
            'contain' => []
        ]);

        $this->set('techniqueSkill', $techniqueSkill);
        $this->set('_serialize', ['techniqueSkill']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->set('title', 'Nsystem｜新規登録');
        $techniqueSkill = $this->TechniqueSkill->newEntity();
        if ($this->request->is('post')) {
            $techniqueSkill = $this->TechniqueSkill->patchEntity($techniqueSkill, $this->request->data);
            if ($this->TechniqueSkill->save($techniqueSkill)) {
                $this->Flash->success(__('新規登録が無事に成功しました'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('失敗しました、もう一度やり直してください'));
            }
        }
        $this->set(compact('techniqueSkill'));
        $this->set('_serialize', ['techniqueSkill']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Technique Skill id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->set('title', 'Nsystem｜編集');
        $techniqueSkill = $this->TechniqueSkill->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $techniqueSkill = $this->TechniqueSkill->patchEntity($techniqueSkill, $this->request->data);
            if ($this->TechniqueSkill->save($techniqueSkill)) {
                $this->Flash->success(__('編集が無事に成功しました'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('失敗しました、もう一度やり直してください'));
            }
        }
        $this->set(compact('techniqueSkill'));
        $this->set('_serialize', ['techniqueSkill']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Technique Skill id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $techniqueSkill = $this->TechniqueSkill->get($id);
        if ($this->TechniqueSkill->delete($techniqueSkill)) {
            $this->Flash->success(__('削除が無事に成功しました'));
        } else {
            $this->Flash->error(__('失敗しました、もう一度やり直してください'));
        }

        return $this->redirect(['action' => 'index']);
    }
}