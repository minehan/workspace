<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * TechniquePerformance Controller
 *
 * @property \App\Model\Table\TechniquePerformanceTable $TechniquePerformance
 */
class TechniquePerformanceController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
  public $paginate = [
    'limit' => 10,
    'order' => [
      'techniquePerformance.name' => 'asc'
    ]
  ];

    public function index()
    {
        $this->set('title', 'Nsystem｜技術実績');
        $techniquePerformance = $this->paginate($this->TechniquePerformance);

        $this->set(compact('techniquePerformance'));
        $this->set('_serialize', ['techniquePerformance']);
    }

    /**
     * View method
     *
     * @param string|null $id Technique Performance id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->set('title', 'Nsystem｜閲覧');
        $techniquePerformance = $this->TechniquePerformance->get($id, [
            'contain' => []
        ]);

        $this->set('techniquePerformance', $techniquePerformance);
        $this->set('_serialize', ['techniquePerformance']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->set('title', 'Nsystem｜新規登録');
        $techniquePerformance = $this->TechniquePerformance->newEntity();
        if ($this->request->is('post')) {
            $techniquePerformance = $this->TechniquePerformance->patchEntity($techniquePerformance, $this->request->data);
            if ($this->TechniquePerformance->save($techniquePerformance)) {
                $this->Flash->success(__('新規登録が無事に成功しました'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('失敗しました、もう一度やり直してください'));
            }
        }
        $this->set(compact('techniquePerformance'));
        $this->set('_serialize', ['techniquePerformance']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Technique Performance id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->set('title', 'Nsystem｜編集');
        $techniquePerformance = $this->TechniquePerformance->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $techniquePerformance = $this->TechniquePerformance->patchEntity($techniquePerformance, $this->request->data);
            if ($this->TechniquePerformance->save($techniquePerformance)) {
                $this->Flash->success(__('編集が無事に成功しました'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('失敗しました、もう一度やり直してください'));
            }
        }
        $this->set(compact('techniquePerformance'));
        $this->set('_serialize', ['techniquePerformance']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Technique Performance id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $techniquePerformance = $this->TechniquePerformance->get($id);
        if ($this->TechniquePerformance->delete($techniquePerformance)) {
            $this->Flash->success(__('削除が無事に成功しました'));
        } else {
            $this->Flash->error(__('失敗しました、もう一度やり直してください'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
