<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * TechniqueSkillNetwork Controller
 *
 * @property \App\Model\Table\TechniqueSkillNetworkTable $TechniqueSkillNetwork
 */
class TechniqueSkillNetworkController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
  public $paginate = [
    'limit' => 10,
    'order' => [
      'techniqueSkillNetwork.name' => 'asc'
    ]
  ];

    public function index()
    {
        $this->set('title', 'Nsystem｜ネットワークスキル');
        $techniqueSkillNetwork = $this->paginate($this->TechniqueSkillNetwork);

        $this->set(compact('techniqueSkillNetwork'));
        $this->set('_serialize', ['techniqueSkillNetwork']);
    }

    /**
     * View method
     *
     * @param string|null $id Technique Skill Network id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->set('title', 'Nsystem｜閲覧');
        $techniqueSkillNetwork = $this->TechniqueSkillNetwork->get($id, [
            'contain' => []
        ]);

        $this->set('techniqueSkillNetwork', $techniqueSkillNetwork);
        $this->set('_serialize', ['techniqueSkillNetwork']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->set('title', 'Nsystem｜新規登録');
        $techniqueSkillNetwork = $this->TechniqueSkillNetwork->newEntity();
        if ($this->request->is('post')) {
            $techniqueSkillNetwork = $this->TechniqueSkillNetwork->patchEntity($techniqueSkillNetwork, $this->request->data);
            if ($this->TechniqueSkillNetwork->save($techniqueSkillNetwork)) {
                $this->Flash->success(__('新規登録が無事に成功しました'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('失敗しました、もう一度やり直してください'));
            }
        }
        $this->set(compact('techniqueSkillNetwork'));
        $this->set('_serialize', ['techniqueSkillNetwork']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Technique Skill Network id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->set('title', 'Nsystem｜編集');
        $techniqueSkillNetwork = $this->TechniqueSkillNetwork->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $techniqueSkillNetwork = $this->TechniqueSkillNetwork->patchEntity($techniqueSkillNetwork, $this->request->data);
            if ($this->TechniqueSkillNetwork->save($techniqueSkillNetwork)) {
                $this->Flash->success(__('編集が無事に成功しました'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('失敗しました、もう一度やり直してください'));
            }
        }
        $this->set(compact('techniqueSkillNetwork'));
        $this->set('_serialize', ['techniqueSkillNetwork']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Technique Skill Network id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $techniqueSkillNetwork = $this->TechniqueSkillNetwork->get($id);
        if ($this->TechniqueSkillNetwork->delete($techniqueSkillNetwork)) {
            $this->Flash->success(__('削除が無事に成功しました'));
        } else {
            $this->Flash->error(__('失敗しました、もう一度やり直してください'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
