<?php
namespace App\Validation;
use Cake\Validation\Validation;

//クラス作るための決まり文句みたいなもの？
class CustomValidation extends Validation
{

  //Cake\Validation\Validationを参考に今回使うものを作成
  public static function number($value, $context){
    return (bool) preg_match('/^[abc]{1}[0-9]{3,4}$/i', $value);
  }

}