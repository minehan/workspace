<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Background Model
 *
 * @method \App\Model\Entity\Background get($primaryKey, $options = [])
 * @method \App\Model\Entity\Background newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Background[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Background|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Background patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Background[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Background findOrCreate($search, callable $callback = null)
 */
class BackgroundTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('background');
        $this->displayField('id');
        $this->primaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator->provider('custom', 'App\Validation\CustomValidation');
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('number', 'create')
            ->notEmpty('number')
            ->add('number', 'custom', [
              'rule' => 'number',
              'provider' => 'custom',
              'message' => 'A~Cを頭につけた数字3~4文字を半角で入力してください'
            ]);

      //文字数どのぐらいが最適か分からないので、とりあえず500文字
        $validator
            ->requirePresence('past', 'create')
            ->notEmpty('past')
            ->add('past','length',[
              'rule' => [
                'maxLength',500
              ],
              'message' => '500文字以内でお願いします'
            ]);

      $validator
        ->requirePresence('achievement', 'create')
        ->allowEmpty('achievement')
        ->add('achievement','length',[
          'rule' => [
            'maxLength',500
          ],
          'message' => '500文字以内でお願いします'
        ]);

        return $validator;
    }
}
