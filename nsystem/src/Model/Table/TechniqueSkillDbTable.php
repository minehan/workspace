<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TechniqueSkillDb Model
 *
 * @method \App\Model\Entity\TechniqueSkillDb get($primaryKey, $options = [])
 * @method \App\Model\Entity\TechniqueSkillDb newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TechniqueSkillDb[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TechniqueSkillDb|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TechniqueSkillDb patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TechniqueSkillDb[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TechniqueSkillDb findOrCreate($search, callable $callback = null)
 */
class TechniqueSkillDbTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('technique_skill_db');
        $this->displayField('id');
        $this->primaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator->provider('custom', 'App\Validation\CustomValidation');
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
          ->requirePresence('number', 'create')
          ->notEmpty('number')
          ->add('number', 'custom', [
            'rule' => 'number',
            'provider' => 'custom',
            'message' => 'A~Cを頭につけた数字3~4文字を半角で入力してください'
          ]);

        $validator
            ->requirePresence('mysql', 'create')
            ->notEmpty('mysql');

        $validator
            ->requirePresence('postgresql', 'create')
            ->notEmpty('postgresql');

        $validator
            ->requirePresence('oracle', 'create')
            ->notEmpty('oracle');

        $validator
            ->requirePresence('sqlserver', 'create')
            ->notEmpty('sqlserver');

        $validator
            ->requirePresence('mongodb', 'create')
            ->notEmpty('mongodb');

        $validator
            ->requirePresence('sqlite', 'create')
            ->notEmpty('sqlite');

        $validator
            ->requirePresence('cassandra', 'create')
            ->notEmpty('cassandra');

      $validator
        ->requirePresence('other', 'create')
        ->allowEmpty('other')
        ->add('other','length',[
          'rule' => [
            'maxLength',500
          ],
          'message' => '500文字以内でお願いします'
        ]);

        return $validator;
    }
}
