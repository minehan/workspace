<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TechniqueSkillLanguage Model
 *
 * @method \App\Model\Entity\TechniqueSkillLanguage get($primaryKey, $options = [])
 * @method \App\Model\Entity\TechniqueSkillLanguage newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TechniqueSkillLanguage[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TechniqueSkillLanguage|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TechniqueSkillLanguage patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TechniqueSkillLanguage[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TechniqueSkillLanguage findOrCreate($search, callable $callback = null)
 */
class TechniqueSkillLanguageTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('technique_skill_language');
        $this->displayField('id');
        $this->primaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator->provider('custom', 'App\Validation\CustomValidation');
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
          ->requirePresence('number', 'create')
          ->notEmpty('number')
          ->add('number', 'custom', [
            'rule' => 'number',
            'provider' => 'custom',
            'message' => 'A~Cを頭につけた数字3~4文字を半角で入力してください'
          ]);

        $validator
            ->requirePresence('c', 'create')
            ->notEmpty('c');

        $validator
            ->requirePresence('c_purapura', 'create')
            ->notEmpty('c_purapura');

        $validator
            ->requirePresence('c_sharp', 'create')
            ->notEmpty('c_sharp');

        $validator
            ->requirePresence('java', 'create')
            ->notEmpty('java');

        $validator
            ->requirePresence('javascript', 'create')
            ->notEmpty('javascript');

        $validator
            ->requirePresence('php', 'create')
            ->notEmpty('php');

        $validator
            ->requirePresence('ruby', 'create')
            ->notEmpty('ruby');

        $validator
            ->requirePresence('perl', 'create')
            ->notEmpty('perl');

        $validator
            ->requirePresence('python', 'create')
            ->notEmpty('python');

        $validator
            ->requirePresence('objective_c', 'create')
            ->notEmpty('objective_c');

        $validator
            ->requirePresence('swift', 'create')
            ->notEmpty('swift');

        $validator
            ->requirePresence('scala', 'create')
            ->notEmpty('scala');

        $validator
            ->requirePresence('go', 'create')
            ->notEmpty('go');

        $validator
            ->requirePresence('html', 'create')
            ->notEmpty('html');

        $validator
            ->requirePresence('css', 'create')
            ->notEmpty('css');

        $validator
            ->requirePresence('html5', 'create')
            ->notEmpty('html5');

        $validator
            ->requirePresence('css3', 'create')
            ->notEmpty('css3');

        $validator
            ->requirePresence('xhtml', 'create')
            ->notEmpty('xhtml');

        $validator
          ->requirePresence('other', 'create')
          ->allowEmpty('other')
          ->add('other','length',[
            'rule' => [
              'maxLength',500
            ],
            'message' => '500文字以内でお願いします'
          ]);

        return $validator;
    }
}
