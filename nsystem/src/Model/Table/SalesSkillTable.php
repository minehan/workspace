<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SalesSkill Model
 *
 * @method \App\Model\Entity\SalesSkill get($primaryKey, $options = [])
 * @method \App\Model\Entity\SalesSkill newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\SalesSkill[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SalesSkill|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SalesSkill patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SalesSkill[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\SalesSkill findOrCreate($search, callable $callback = null)
 */
class SalesSkillTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('sales_skill');
        $this->displayField('id');
        $this->primaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator->provider('custom', 'App\Validation\CustomValidation');
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
          ->requirePresence('number', 'create')
          ->notEmpty('number')
          ->add('number', 'custom', [
            'rule' => 'number',
            'provider' => 'custom',
            'message' => 'A~Cを頭につけた数字3~4文字を半角で入力してください'
          ]);

        $validator
            ->requirePresence('drivers_license', 'create')
            ->notEmpty('drivers_license');

        $validator
            ->requirePresence('mos', 'create')
            ->notEmpty('mos');

        $validator
            ->requirePresence('fp', 'create')
            ->notEmpty('fp');

        $validator
            ->requirePresence('taken', 'create')
            ->notEmpty('taken');

        $validator
            ->requirePresence('sale', 'create')
            ->notEmpty('sale');

        $validator
            ->requirePresence('bookkeeping', 'create')
            ->notEmpty('bookkeeping');

        $validator
            ->requirePresence('basic', 'create')
            ->notEmpty('basic');

        $validator
            ->integer('toeic')
            ->requirePresence('toeic', 'create')
            ->allowEmpty('toeic')
            ->add('toeic', 'length',[
              'rule' => [
                'maxLength',3
              ],
              'message' => 'ありえない点数が入力されています'
            ]);

        $validator
            ->requirePresence('chinese', 'create')
            ->notEmpty('chinese')
            ->add('chinese', 'length',[
              'rule' => [
                'maxLength',100
              ],
              'message' => '詳細は100文字以内でお願いします'
            ]);

      $validator
        ->requirePresence('other', 'create')
        ->allowEmpty('other')
        ->add('other','length',[
          'rule' => [
            'maxLength',500
          ],
          'message' => '500文字以内でお願いします'
        ]);

        return $validator;
    }
}
