<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TechniqueSkillServer Model
 *
 * @method \App\Model\Entity\TechniqueSkillServer get($primaryKey, $options = [])
 * @method \App\Model\Entity\TechniqueSkillServer newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TechniqueSkillServer[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TechniqueSkillServer|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TechniqueSkillServer patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TechniqueSkillServer[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TechniqueSkillServer findOrCreate($search, callable $callback = null)
 */
class TechniqueSkillServerTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('technique_skill_server');
        $this->displayField('id');
        $this->primaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator->provider('custom', 'App\Validation\CustomValidation');
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
          ->requirePresence('number', 'create')
          ->notEmpty('number')
          ->add('number', 'custom', [
            'rule' => 'number',
            'provider' => 'custom',
            'message' => 'A~Cを頭につけた数字3~4文字を半角で入力してください'
          ]);

        $validator
            ->requirePresence('iis', 'create')
            ->notEmpty('iis');

        $validator
            ->requirePresence('apache', 'create')
            ->notEmpty('apache');

        $validator
            ->requirePresence('lighttpd', 'create')
            ->notEmpty('lighttpd');

        $validator
            ->requirePresence('sun', 'create')
            ->notEmpty('sun');

        $validator
            ->requirePresence('nignx', 'create')
            ->notEmpty('nignx');

        $validator
            ->requirePresence('tomcat', 'create')
            ->notEmpty('tomcat');

        $validator
            ->requirePresence('webrick', 'create')
            ->notEmpty('webrick');

        $validator
            ->requirePresence('cherrypy', 'create')
            ->notEmpty('cherrypy');

        $validator
            ->requirePresence('postfix', 'create')
            ->notEmpty('postfix');

        $validator
            ->requirePresence('sendmail', 'create')
            ->notEmpty('sendmail');

        $validator
            ->requirePresence('qmail', 'create')
            ->notEmpty('qmail');

        $validator
          ->requirePresence('other', 'create')
          ->allowEmpty('other')
          ->add('other','length',[
            'rule' => [
              'maxLength',500
            ],
            'message' => '500文字以内でお願いします'
          ]);

        return $validator;
    }
}
