<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TechniqueSkill Model
 *
 * @method \App\Model\Entity\TechniqueSkill get($primaryKey, $options = [])
 * @method \App\Model\Entity\TechniqueSkill newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TechniqueSkill[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TechniqueSkill|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TechniqueSkill patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TechniqueSkill[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TechniqueSkill findOrCreate($search, callable $callback = null)
 */
class TechniqueSkillTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('technique_skill');
        $this->displayField('id');
        $this->primaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator->provider('custom', 'App\Validation\CustomValidation');
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('number', 'create')
            ->notEmpty('number')
            ->add('number', 'custom', [
              'rule' => 'number',
              'provider' => 'custom',
              'message' => 'A~Cを頭につけた数字3~4文字を半角で入力してください'
            ]);

        $validator
            ->requirePresence('application', 'create')
            ->notEmpty('application');

        $validator
            ->requirePresence('moe', 'create')
            ->notEmpty('moe');

        $validator
            ->requirePresence('oracle_master', 'create')
            ->notEmpty('oracle_master');

        $validator
            ->requirePresence('ne_specialist', 'create')
            ->notEmpty('ne_specialist');

        $validator
            ->requirePresence('db_specialist', 'create')
            ->notEmpty('db_specialist');

        $validator
            ->requirePresence('security_specialist', 'create')
            ->notEmpty('security_specialist');

        $validator
            ->requirePresence('projectmanager', 'create')
            ->notEmpty('projectmanager');

        $validator
            ->requirePresence('itstrategist', 'create')
            ->notEmpty('itstrategist');

        $validator
            ->requirePresence('ccnp', 'create')
            ->notEmpty('ccnp');

        $validator
            ->requirePresence('ccie', 'create')
            ->notEmpty('ccie');

        $validator
            ->requirePresence('lpic', 'create')
            ->notEmpty('lpic');

        $validator
            ->requirePresence('au', 'create')
            ->notEmpty('au');

      $validator
        ->integer('toeic')
        ->requirePresence('toeic', 'create')
        ->allowEmpty('toeic')
        ->add('toeic', 'length',[
          'rule' => [
            'maxLength',3
          ],
          'message' => 'ありえない点数が入力されています'
        ]);

      $validator
        ->requirePresence('other', 'create')
        ->allowEmpty('other')
        ->add('other','length',[
          'rule' => [
            'maxLength',500
          ],
          'message' => '500文字以内でお願いします'
        ]);

        return $validator;
    }
}
