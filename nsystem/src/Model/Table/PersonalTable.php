<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
//use App\Model\Entity\Personal;
use Cake\Validation\Validator;

/**
 * Personal Model
 *
 * @method \App\Model\Entity\Personal get($primaryKey, $options = [])
 * @method \App\Model\Entity\Personal newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Personal[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Personal|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Personal patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Personal[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Personal findOrCreate($search, callable $callback = null)
 */
class PersonalTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

      $this->table('personal');
      $this->displayField('name');
      $this->primaryKey('id');
      //画像アップロードのプラグインの設定
      $this->addBehavior('Josegonzalez/Upload.Upload', [
        'photo' => [
          'fields' => [
            'dir' => 'photo_dir'
          ],
        ],
      ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
      $validator->provider('custom', 'App\Validation\CustomValidation');
      $validator
          ->integer('id')
          ->allowEmpty('id', 'create');

      $validator
        ->requirePresence('number', 'create')
        ->notEmpty('number')
        ->add('number', 'custom', [
          'rule' => 'number',
          'provider' => 'custom',
          'message' => 'A~Cを頭につけた数字3~4文字を半角で入力してください'
        ]);

      $validator
          ->notEmpty('name');

      $validator
        ->allowEmpty('photo')
        ->add('photo', 'validExtension', [
          'rule' => ['extension',[
              'gif',
              'jpeg',
              'png',
              'jpg'
            ]]
        ]);

      $validator
          ->allowEmpty('photo_dir');

      $validator
        ->integer('age')
        ->requirePresence('age', 'create')
        ->notEmpty('age')
        ->add('age', 'comparison', [
          'rule' => [
            'comparison','>',17
          ],
          'message' => 'ありえない年齢が入力されています'
        ]);

      $validator
          ->notEmpty('sex');

      $validator
        ->allowEmpty('mail')
        ->add('mail', 'vaidFormat', [
          'rule' => 'email',
          'message' => 'メールアドレスが正しく入力されていません'
        ]);

      $validator
        ->integer('tel')
        ->requirePresence('tel', 'create')
        ->allowEmpty('tel')
        ->add('tel', 'lengthBetween', [
          'rule' => [
            'lengthBetween',11,12
          ],
          'message' => '電話番号が正しく入力されていません'
        ]);

      $validator
          ->notEmpty('address');

      $validator
        ->integer('salary')
        ->requirePresence('salary', 'create')
        ->allowEmpty('salary')
        ->add('salary','length',[
          'rule' => [
            'maxLength',8
          ],
          'message' => 'ありえない金額が入力されています'
        ]);

      $validator
        ->requirePresence('post', 'create')
        ->allowEmpty('post')
        ->add('post','length',[
          'rule' => [
            'maxLength',100
          ],
          'message' => '詳細は100文字以内でお願いします'
        ]);

      $validator
        ->requirePresence('family', 'create')
        ->allowEmpty('family')
        ->add('family','length',[
          'rule' => [
            'maxLength',100
          ],
          'message' => '詳細は100文字以内でお願いします'
        ]);

      $validator
          ->date('startday')
          ->notEmpty('startday');

      $validator
        ->requirePresence('position', 'create')
        ->allowEmpty('position')
        ->add('position','length',[
          'rule' => [
            'maxLength',100
          ],
          'message' => '詳細は100文字以内でお願いします'
        ]);

      return $validator;
    }
}
