<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SalesPerformance Model
 *
 * @method \App\Model\Entity\SalesPerformance get($primaryKey, $options = [])
 * @method \App\Model\Entity\SalesPerformance newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\SalesPerformance[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SalesPerformance|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SalesPerformance patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SalesPerformance[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\SalesPerformance findOrCreate($search, callable $callback = null)
 */
class SalesPerformanceTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('sales_performance');
        $this->displayField('id');
        $this->primaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator->provider('custom', 'App\Validation\CustomValidation');
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
          ->requirePresence('number', 'create')
          ->notEmpty('number')
          ->add('number', 'custom', [
            'rule' => 'number',
            'provider' => 'custom',
            'message' => 'A~Cを頭につけた数字3~4文字を半角で入力してください'
          ]);

        $validator
            ->integer('sales')
            ->requirePresence('sales', 'create')
            ->allowEmpty('sales')
            ->add('sales','length',[
              'rule' => [
                'maxLength',10
              ],
              'message' => 'ありえない金額が入力されています'
            ]);

        $validator
            ->date('month')
            ->requirePresence('month', 'create')
            ->notEmpty('month');

      $validator
        ->requirePresence('item', 'create')
        ->allowEmpty('item')
        ->add('item','length',[
          'rule' => [
            'maxLength',500
          ],
          'message' => '500文字以内でお願いします'
        ]);

        return $validator;
    }
}
