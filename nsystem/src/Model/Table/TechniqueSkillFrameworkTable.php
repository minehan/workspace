<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TechniqueSkillFramework Model
 *
 * @method \App\Model\Entity\TechniqueSkillFramework get($primaryKey, $options = [])
 * @method \App\Model\Entity\TechniqueSkillFramework newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TechniqueSkillFramework[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TechniqueSkillFramework|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TechniqueSkillFramework patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TechniqueSkillFramework[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TechniqueSkillFramework findOrCreate($search, callable $callback = null)
 */
class TechniqueSkillFrameworkTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('technique_skill_framework');
        $this->displayField('id');
        $this->primaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator->provider('custom', 'App\Validation\CustomValidation');
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

      $validator
        ->requirePresence('number', 'create')
        ->notEmpty('number')
        ->add('number', 'custom', [
          'rule' => 'number',
          'provider' => 'custom',
          'message' => 'A~Cを頭につけた数字3~4文字を半角で入力してください'
        ]);

        $validator
            ->requirePresence('cakephp', 'create')
            ->notEmpty('cakephp');

        $validator
            ->requirePresence('symfony', 'create')
            ->notEmpty('symfony');

        $validator
            ->requirePresence('ruby_on_rails', 'create')
            ->notEmpty('ruby_on_rails');

        $validator
            ->requirePresence('sinatra', 'create')
            ->notEmpty('sinatra');

        $validator
            ->requirePresence('django', 'create')
            ->notEmpty('django');

        $validator
            ->requirePresence('bottle', 'create')
            ->notEmpty('bottle');

        $validator
            ->requirePresence('jquery', 'create')
            ->notEmpty('jquery');

        $validator
            ->requirePresence('angular_js', 'create')
            ->notEmpty('angular_js');

        $validator
            ->requirePresence('backbone_js', 'create')
            ->notEmpty('backbone_js');

        $validator
            ->requirePresence('node_js', 'create')
            ->notEmpty('node_js');

        $validator
            ->requirePresence('apachestruts', 'create')
            ->notEmpty('apachestruts');

        $validator
            ->requirePresence('springframework', 'create')
            ->notEmpty('springframework');

        $validator
            ->requirePresence('dott_net', 'create')
            ->notEmpty('dott_net');

        $validator
            ->requirePresence('cocoaframework', 'create')
            ->notEmpty('cocoaframework');

        $validator
            ->requirePresence('kitura', 'create')
            ->notEmpty('kitura');

        $validator
            ->requirePresence('jsp', 'create')
            ->notEmpty('jsp');

        $validator
            ->requirePresence('cordova', 'create')
            ->notEmpty('cordova');

        $validator
            ->requirePresence('unity', 'create')
            ->notEmpty('unity');

        $validator
          ->requirePresence('other', 'create')
          ->allowEmpty('other')
          ->add('other','length',[
            'rule' => [
              'maxLength',500
            ],
            'message' => '500文字以内でお願いします'
          ]);

        return $validator;
    }
}
