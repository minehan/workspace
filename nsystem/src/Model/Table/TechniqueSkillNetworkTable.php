<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TechniqueSkillNetwork Model
 *
 * @method \App\Model\Entity\TechniqueSkillNetwork get($primaryKey, $options = [])
 * @method \App\Model\Entity\TechniqueSkillNetwork newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TechniqueSkillNetwork[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TechniqueSkillNetwork|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TechniqueSkillNetwork patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TechniqueSkillNetwork[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TechniqueSkillNetwork findOrCreate($search, callable $callback = null)
 */
class TechniqueSkillNetworkTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('technique_skill_network');
        $this->displayField('id');
        $this->primaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator->provider('custom', 'App\Validation\CustomValidation');
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
          ->requirePresence('number', 'create')
          ->notEmpty('number')
          ->add('number', 'custom', [
            'rule' => 'number',
            'provider' => 'custom',
            'message' => 'A~Cを頭につけた数字3~4文字を半角で入力してください'
          ]);

        $validator
            ->requirePresence('cisco', 'create')
            ->notEmpty('cisco');

        $validator
            ->requirePresence('juniper', 'create')
            ->notEmpty('juniper');

        $validator
            ->requirePresence('f5', 'create')
            ->notEmpty('f5');

        $validator
            ->requirePresence('a10', 'create')
            ->notEmpty('a10');

        $validator
            ->requirePresence('alaxala', 'create')
            ->notEmpty('alaxala');

        $validator
            ->requirePresence('yamaha', 'create')
            ->notEmpty('yamaha');

        $validator
            ->requirePresence('radware', 'create')
            ->notEmpty('radware');

        $validator
            ->requirePresence('fortigate', 'create')
            ->notEmpty('fortigate');

        $validator
            ->requirePresence('apresia', 'create')
            ->notEmpty('apresia');

        $validator
            ->requirePresence('infoblox', 'create')
            ->notEmpty('infoblox');

        $validator
            ->requirePresence('paloalto', 'create')
            ->notEmpty('paloalto');

        $validator
            ->requirePresence('powerconnect', 'create')
            ->notEmpty('powerconnect');

        $validator
            ->requirePresence('bulecoat', 'create')
            ->notEmpty('bulecoat');

        $validator
            ->requirePresence('bash', 'create')
            ->notEmpty('bash');

        $validator
            ->requirePresence('zsh', 'create')
            ->notEmpty('zsh');

        $validator
            ->requirePresence('csh', 'create')
            ->notEmpty('csh');

        $validator
            ->requirePresence('tcsh', 'create')
            ->notEmpty('tcsh');

        $validator
          ->requirePresence('other', 'create')
          ->allowEmpty('other')
          ->add('other','length',[
            'rule' => [
              'maxLength',500
            ],
            'message' => '500文字以内でお願いします'
          ]);

        return $validator;
    }
}
