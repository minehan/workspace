<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TechniqueSkillDesign Model
 *
 * @method \App\Model\Entity\TechniqueSkillDesign get($primaryKey, $options = [])
 * @method \App\Model\Entity\TechniqueSkillDesign newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TechniqueSkillDesign[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TechniqueSkillDesign|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TechniqueSkillDesign patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TechniqueSkillDesign[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TechniqueSkillDesign findOrCreate($search, callable $callback = null)
 */
class TechniqueSkillDesignTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('technique_skill_design');
        $this->displayField('id');
        $this->primaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator->provider('custom', 'App\Validation\CustomValidation');
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
          ->requirePresence('number', 'create')
          ->notEmpty('number')
          ->add('number', 'custom', [
            'rule' => 'number',
            'provider' => 'custom',
            'message' => 'A~Cを頭につけた数字3~4文字を半角で入力してください'
          ]);

        $validator
            ->requirePresence('photoshop', 'create')
            ->notEmpty('photoshop');

        $validator
            ->requirePresence('illsutrator', 'create')
            ->notEmpty('illsutrator');

        $validator
            ->requirePresence('indesign', 'create')
            ->notEmpty('indesign');

        $validator
            ->requirePresence('maya', 'create')
            ->notEmpty('maya');

        $validator
            ->requirePresence('threedsmax', 'create')
            ->notEmpty('threedsmax');

        $validator
            ->requirePresence('lightwave3d', 'create')
            ->notEmpty('lightwave3d');

        $validator
            ->requirePresence('edius', 'create')
            ->notEmpty('edius');

        $validator
            ->requirePresence('premiere', 'create')
            ->notEmpty('premiere');

        $validator
            ->requirePresence('vagas', 'create')
            ->notEmpty('vagas');

      $validator
        ->requirePresence('other', 'create')
        ->allowEmpty('other')
        ->add('other','length',[
          'rule' => [
            'maxLength',500
          ],
          'message' => '500文字以内でお願いします'
        ]);

        return $validator;
    }
}
