<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Personal Entity
 *
 * @property int $id
 * @property string $number
 * @property string $name
 * @property string $photo
 * @property string $photo_dir
 * @property int $age
 * @property string $sex
 * @property string $mail
 * @property string $tel
 * @property string $address
 * @property int $salary
 * @property string $post
 * @property string $family
 * @property \Cake\I18n\Time $startday
 * @property string $position
 */
class Personal extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
