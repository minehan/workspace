<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TechniqueSkillLanguage Entity
 *
 * @property int $id
 * @property string $number
 * @property string $c
 * @property string $c_purapura
 * @property string $c_sharp
 * @property string $java
 * @property string $javascript
 * @property string $php
 * @property string $ruby
 * @property string $perl
 * @property string $python
 * @property string $objective_c
 * @property string $swift
 * @property string $scala
 * @property string $go
 * @property string $html
 * @property string $css
 * @property string $html5
 * @property string $css3
 * @property string $xhtml
 * @property string $other
 */
class TechniqueSkillLanguage extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
