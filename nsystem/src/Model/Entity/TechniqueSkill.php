<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TechniqueSkill Entity
 *
 * @property int $id
 * @property string $number
 * @property string $application
 * @property int $toeic
 * @property string $moe
 * @property string $oracle_master
 * @property string $ne_specialist
 * @property string $db_specialist
 * @property string $security_specialist
 * @property string $projectmanager
 * @property string $itstrategist
 * @property string $ccnp
 * @property string $ccie
 * @property string $lpic
 * @property string $au
 * @property string $other
 */
class TechniqueSkill extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
