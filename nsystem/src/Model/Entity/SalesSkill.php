<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SalesSkill Entity
 *
 * @property int $id
 * @property string $number
 * @property string $drivers_license
 * @property string $mos
 * @property string $fp
 * @property string $taken
 * @property string $sale
 * @property string $bookkeeping
 * @property string $basic
 * @property int $toeic
 * @property string $chinese
 * @property string $other
 */
class SalesSkill extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
