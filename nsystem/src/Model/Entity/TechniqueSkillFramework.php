<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TechniqueSkillFramework Entity
 *
 * @property int $id
 * @property string $number
 * @property string $cakephp
 * @property string $symfony
 * @property string $ruby_on_rails
 * @property string $sinatra
 * @property string $django
 * @property string $bottle
 * @property string $jquery
 * @property string $angular_js
 * @property string $backbone_js
 * @property string $node_js
 * @property string $apachestruts
 * @property string $springframework
 * @property string $dott_net
 * @property string $cocoaframework
 * @property string $kitura
 * @property string $jsp
 * @property string $cordova
 * @property string $unity
 * @property string $other
 */
class TechniqueSkillFramework extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
