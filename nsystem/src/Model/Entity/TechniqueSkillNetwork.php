<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TechniqueSkillNetwork Entity
 *
 * @property int $id
 * @property string $number
 * @property string $cisco
 * @property string $juniper
 * @property string $f5
 * @property string $a10
 * @property string $alaxala
 * @property string $yamaha
 * @property string $radware
 * @property string $fortigate
 * @property string $apresia
 * @property string $infoblox
 * @property string $paloalto
 * @property string $powerconnect
 * @property string $bulecoat
 * @property string $bash
 * @property string $zsh
 * @property string $csh
 * @property string $tcsh
 * @property string $other
 */
class TechniqueSkillNetwork extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
