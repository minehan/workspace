<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TechniqueSkillServer Entity
 *
 * @property int $id
 * @property string $number
 * @property string $iis
 * @property string $apache
 * @property string $lighttpd
 * @property string $sun
 * @property string $nignx
 * @property string $tomcat
 * @property string $webrick
 * @property string $cherrypy
 * @property string $postfix
 * @property string $sendmail
 * @property string $qmail
 * @property string $other
 */
class TechniqueSkillServer extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
