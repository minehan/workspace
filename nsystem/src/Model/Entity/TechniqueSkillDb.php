<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TechniqueSkillDb Entity
 *
 * @property int $id
 * @property string $number
 * @property string $mysql
 * @property string $postgresql
 * @property string $oracle
 * @property string $sqlserver
 * @property string $mongodb
 * @property string $sqlite
 * @property string $cassandra
 * @property string $other
 */
class TechniqueSkillDb extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
