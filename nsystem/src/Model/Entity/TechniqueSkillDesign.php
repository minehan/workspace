<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TechniqueSkillDesign Entity
 *
 * @property int $id
 * @property string $number
 * @property string $photoshop
 * @property string $illsutrator
 * @property string $indesign
 * @property string $maya
 * @property string $threedsmax
 * @property string $lightwave3d
 * @property string $edius
 * @property string $premiere
 * @property string $vagas
 * @property string $other
 */
class TechniqueSkillDesign extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
