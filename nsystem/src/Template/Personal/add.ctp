<div class="wrapper">
  <div class="leftNavi">
    <nav>
      <h1 class="home02">
        <a href="<?php echo($this->Url->build(["controller" => "Users", "action" => "home"])); ?>">
          <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#logo" /></svg>NSYSTEM
        </a>
      </h1>
      <div class="leftContents">
        <h2>アクション</h2>
        <ul>
          <li><a href="<?php echo($this->Url->build(["controller" => "Personal", "action" => "index"])); ?>">
            <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#user" /></svg>一覧ページへ</a></li>
        </ul>
        <hr width="90%" align="right" color="#f60">
      </div>
      <ul id="dropDown">
        <li id="menu01"><a href="#dd01" class="notLink"><svg role="image" class="icon"><use xlink:href="/svg/icons.svg#office" /></svg>共通の登録</a>
          <ul id="dd01">
            <li><a href="<?php echo($this->Url->build(["controller" => "Personal", "action" => "index"])); ?>">
              <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#address-book" /></svg>個人情報</a></li>
            <li><a href="<?php echo($this->Url->build(["controller" => "Background", "action" => "index"])); ?>">
              <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#file-text2" /></svg>経歴</a></li>
          </ul>
        </li>
        <li id="menu02"><a href="#dd02"  class="notLink"><svg role="image" class="icon"><use xlink:href="/svg/icons.svg#bubbles2" /></svg>営業社員の登録</a>
          <ul id="dd02">
            <li><a href="<?php echo($this->Url->build(["controller" => "SalesPerformance", "action" => "index"])); ?>">
              <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#trophy" /></svg>営業実績</a></li>
            <li><a href="<?php echo($this->Url->build(["controller" => "SalesSkill", "action" => "index"])); ?>">
              <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#pencil" /></svg>営業資格一覧</a></li>
          </ul>
        </li>
        <li id="menu03"><a href="#dd03"  class="notLink"><svg role="image" class="icon"><use xlink:href="/svg/icons.svg#wrench" /></svg>技術社員の登録</a>
          <ul id="dd03">
            <li><a href="<?php echo($this->Url->build(["controller" => "TechniquePerformance", "action" => "index"])); ?>">
              <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#trophy" /></svg>技術実績</a></li>
            <li><a href="<?php echo($this->Url->build(["controller" => "TechniqueSkill", "action" => "index"])); ?>">
              <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#pencil" /></svg>技術資格一覧</a></li>
            <li><a href="<?php echo($this->Url->build(["controller" => "TechniqueSkillDb", "action" => "index"])); ?>">
              <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#database" /></svg>データベーススキル</a></li>
            <li><a href="<?php echo($this->Url->build(["controller" => "TechniqueSkillDesign", "action" => "index"])); ?>">
              <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#quill" /></svg>デザインスキル</a></li>
            <li><a href="<?php echo($this->Url->build(["controller" => "TechniqueSkillFramework", "action" => "index"])); ?>">
              <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#magic-wand" /></svg>フレームワークスキル</a></li>
            <li><a href="<?php echo($this->Url->build(["controller" => "TechniqueSkillLanguage", "action" => "index"])); ?>">
              <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#embed2" /></svg>プログラミングスキル</a></li>
            <li><a href="<?php echo($this->Url->build(["controller" => "techniqueSkillNetwork", "action" => "index"])); ?>">
              <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#sphere" /></svg>ネットワークスキル</a></li>
            <li><a href="<?php echo($this->Url->build(["controller" => "TechniqueSkillServer", "action" => "index"])); ?>">
              <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#terminal" /></svg>サーバースキル</a></li>
          </ul>
        </li>
      </ul>
    </nav>
  </div>

  <div class="rightNavi">
    <div class="rightContents">
      <h2><span>【新規登録】個人情報</span></h2>
      <div class="addForm">
        <?= $this->Form->create($personal, ['type' => 'file']) ?>
        <?= $this->Form->input('number',[
  'label'=> [
    'text'=>'社員番号'
  ]
]) ?>
        <?= $this->Form->input('name',[
  'label'=> [
    'text'=>'氏名'
  ]
]) ?>
        <?php
        echo '<label>顔写真</label>';
        echo '<label for="photo" class="labelUpload"><svg role="image" class="icon"><use xlink:href="/svg/icons.svg#upload3" /></svg>写真を選択';
        echo '<input type="file" name="photo" id="photo">';
        echo '</label>';
  ?>
        <?= $this->Form->input('photo_dir',[
  'type' => 'hidden'
]) ?>
        <?= $this->Form->input('age',[
  'label'=> [
    'text'=>'年齢'
  ]
]) ?>
        <?php
        echo '<label>性別</label>';
        echo '<input type="radio" name="sex" value="男" checked required="required" id="1_radio" /><label for="1_radio" class="labelRadioButton">男</label>';
        echo '<input type="radio" name="sex" value="女" required="required" id="2_radio" /><label for="2_radio" class="labelRadioButton">女</label>';
  ?>
        <?= $this->Form->input('mail',[
  'label'=> [
    'text'=>'メール'
  ]
]) ?>
        <?= $this->Form->input('tel',[
  'label'=> [
    'text'=>'電話番号'
  ]
]) ?>
        <?= $this->Form->input('address',[
  'label'=> [
    'text'=>'住所'
  ]
]) ?>
        <?= $this->Form->input('salary',[
  'label'=> [
    'text'=>'給料'
  ]
]) ?>
        <?= $this->Form->input('post',[
  'label'=> [
    'text'=>'部署'
  ]
]) ?>
        <?= $this->Form->input('family',[
  'label'=> [
    'text'=>'家族構成'
  ]
]) ?>
        <?php echo '<div class="customSelectbox">'; ?>
        <?php echo '<label>入社日</label>'; ?>
        <?php echo '<div class="inner"><span class="year01"></span></div>'; ?>
        <?php echo '<div class="inner"><span class="month01"></span></div>'; ?>
        <?php echo '<div class="inner"><span class="day01"></span></div>'; ?>
        <?= $this->Form->input('startday',[
  'label'=> false,
  'type'=>'date',
  'dateFormat'=>'YMD',
  'monthNames'=> false,
  'year' =>[
    'class'=>'box01'],
  'month' =>[
    'class'=>'box02'],
  'day'=>[
    'class'=>'box03'],
  'maxYear'=>date('Y'),
  'minYear'=>date('Y') - 20
]) ?>
        <?php echo '</div>'; ?>
        <?= $this->Form->input('position',[
  'label'=> [
    'text'=>'役職'
  ]
]) ?>
        <button type="submit"><svg role="image" class="icon"><use xlink:href="/svg/icons.svg#send" /></svg>送信</button>
        <?= $this->Form->end() ?>
      </div>
    </div>
  </div>
</div>