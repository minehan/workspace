<div class="wrapper">
  <div class="leftNavi">
    <nav>
      <h1 class="home02">
        <a href="<?php echo($this->Url->build(["controller" => "Users", "action" => "home"])); ?>">
          <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#logo" /></svg>NSYSTEM
        </a>
      </h1>
      <div class="leftContents">
        <h2>アクション</h2>
        <ul>
          <li><a href="<?php echo($this->Url->build(["controller" => "SalesSkill", "action" => "index"])); ?>">
            <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#user" /></svg>一覧ページへ</a></li>
          <li><?= $this->Form->postLink('<svg role="image" class="icon"><use xlink:href="/svg/icons.svg#user-minus" /></svg>削除', ['action' => 'delete', $salesSkill->id], ['escape' => false, 'confirm' => __('本当に削除してもよろしいですか?', $salesSkill->id)]) ?></li>
        </ul>
        <hr width="90%" align="right" color="#f60">
      </div>
      <ul id="dropDown">
        <li id="menu01"><a href="#dd01" class="notLink"><svg role="image" class="icon"><use xlink:href="/svg/icons.svg#office" /></svg>共通の登録</a>
          <ul id="dd01">
            <li><a href="<?php echo($this->Url->build(["controller" => "Personal", "action" => "index"])); ?>">
              <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#address-book" /></svg>個人情報</a></li>
            <li><a href="<?php echo($this->Url->build(["controller" => "Background", "action" => "index"])); ?>">
              <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#file-text2" /></svg>経歴</a></li>
          </ul>
        </li>
        <li id="menu02"><a href="#dd02"  class="notLink"><svg role="image" class="icon"><use xlink:href="/svg/icons.svg#bubbles2" /></svg>営業社員の登録</a>
          <ul id="dd02">
            <li><a href="<?php echo($this->Url->build(["controller" => "SalesPerformance", "action" => "index"])); ?>">
              <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#trophy" /></svg>営業実績</a></li>
            <li><a href="<?php echo($this->Url->build(["controller" => "SalesSkill", "action" => "index"])); ?>">
              <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#pencil" /></svg>営業資格一覧</a></li>
          </ul>
        </li>
        <li id="menu03"><a href="#dd03"  class="notLink"><svg role="image" class="icon"><use xlink:href="/svg/icons.svg#wrench" /></svg>技術社員の登録</a>
          <ul id="dd03">
            <li><a href="<?php echo($this->Url->build(["controller" => "TechniquePerformance", "action" => "index"])); ?>">
              <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#trophy" /></svg>技術実績</a></li>
            <li><a href="<?php echo($this->Url->build(["controller" => "TechniqueSkill", "action" => "index"])); ?>">
              <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#pencil" /></svg>技術資格一覧</a></li>
            <li><a href="<?php echo($this->Url->build(["controller" => "TechniqueSkillDb", "action" => "index"])); ?>">
              <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#database" /></svg>データベーススキル</a></li>
            <li><a href="<?php echo($this->Url->build(["controller" => "TechniqueSkillDesign", "action" => "index"])); ?>">
              <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#quill" /></svg>デザインスキル</a></li>
            <li><a href="<?php echo($this->Url->build(["controller" => "TechniqueSkillFramework", "action" => "index"])); ?>">
              <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#magic-wand" /></svg>フレームワークスキル</a></li>
            <li><a href="<?php echo($this->Url->build(["controller" => "TechniqueSkillLanguage", "action" => "index"])); ?>">
              <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#embed2" /></svg>プログラミングスキル</a></li>
            <li><a href="<?php echo($this->Url->build(["controller" => "techniqueSkillNetwork", "action" => "index"])); ?>">
              <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#sphere" /></svg>ネットワークスキル</a></li>
            <li><a href="<?php echo($this->Url->build(["controller" => "TechniqueSkillServer", "action" => "index"])); ?>">
              <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#terminal" /></svg>サーバースキル</a></li>
          </ul>
        </li>
      </ul>
    </nav>
  </div>

  <div class="rightNavi">
    <div class="rightContents">
      <h2><span>【編集】営業資格</span></h2>
      <div class="addForm">
        <?= $this->Form->create($salesSkill) ?>
        <?= $this->Form->input('number',[
  'label'=> [
    'text'=>'社員番号'
  ]
]) ?>
        <!--ラジオボタンはヘルパー使用しないで出力 -->
        <?php
  $arrayLabel = array('運転免許', 'マイクロソフトオフィス', 'ファイナンシャルプランナー', '宅建', '販売士', '簿記','基本情報技術者');
        $arrayName = array('drivers_license', 'mos', 'fp', 'taken', 'sale', 'bookkeeping', 'basic');
        ?>
        <?php for($i = 0; $i < count($arrayName); $i++) : ?>
        <?php $num = $i * 2 ?>
        <label><?php echo $arrayLabel[$i] ?></label>
        <input type="radio" name="<?php echo $arrayName[$i] ?>" value="あり" checked required="required" id="<?php echo $num + 1 . '_radio' ?>" />
        <label for="<?php echo $num + 1 . '_radio' ?>" class="labelRadioButton">あり</label>
        <input type="radio" name="<?php echo $arrayName[$i] ?>" value="なし" required="required" id="<?php echo $num + 2 . '_radio' ?>" />
        <label for="<?php echo $num + 2 . '_radio' ?>" class="labelRadioButton">なし</label>
        <?php endfor ; ?>
        <?= $this->Form->input('toeic',[
  'label'=> [
    'text'=>'TOEIC(半角数字3桁で記入してください)'
  ]
]) ?>
        <?= $this->Form->input('chinese',[
  'label'=> [
    'text'=>'中国語'
  ]
]) ?>
        <?= $this->Form->input('other',[
  'label'=> [
    'text'=>'その他（業務に関係ない資格や所持資格の詳細を記述してください）'
  ]
]) ?>
        <button type="submit"><svg role="image" class="icon"><use xlink:href="/svg/icons.svg#send" /></svg>送信</button>
        <?= $this->Form->end() ?>
      </div>
    </div>
  </div>
</div>
