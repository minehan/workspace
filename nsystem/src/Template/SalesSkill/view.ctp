<div class="wrapper">
  <div class="leftNavi">
    <nav>
      <h1 class="home02">
        <a href="<?php echo($this->Url->build(["controller" => "Users", "action" => "home"])); ?>">
          <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#logo" /></svg>NSYSTEM
        </a>
      </h1>
      <div class="leftContents">
        <h2>アクション</h2>
        <ul>
          <li><a href="<?php echo($this->Url->build(["controller" => "SalesSkill", "action" => "index"])); ?>">
            <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#user" /></svg>一覧ページへ</a></li>
          <li><a href="<?php echo($this->Url->build(["controller" => "SalesSkill", "action" => "edit", $salesSkill->id])); ?>">
            <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#pencil2" /></svg>編集</a></li>
          <li><?= $this->Form->postLink('<svg role="image" class="icon"><use xlink:href="/svg/icons.svg#user-minus" /></svg>削除', ['action' => 'delete', $salesSkill->id], ['escape' => false, 'confirm' => __('本当に削除してもよろしいですか?', $salesSkill->id)]) ?></li>
          <li><a href="<?php echo($this->Url->build(["controller" => "SalesSkill", "action" => "add"])); ?>">
            <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#user-plus" /></svg>新規登録</a></li>
        </ul>
        <hr width="90%" align="right" color="#f60">
      </div>
      <ul id="dropDown">
        <li id="menu01"><a href="#dd01" class="notLink"><svg role="image" class="icon"><use xlink:href="/svg/icons.svg#office" /></svg>共通の登録</a>
          <ul id="dd01">
            <li><a href="<?php echo($this->Url->build(["controller" => "Personal", "action" => "index"])); ?>">
              <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#address-book" /></svg>個人情報</a></li>
            <li><a href="<?php echo($this->Url->build(["controller" => "Background", "action" => "index"])); ?>">
              <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#file-text2" /></svg>経歴</a></li>
          </ul>
        </li>
        <li id="menu02"><a href="#dd02"  class="notLink"><svg role="image" class="icon"><use xlink:href="/svg/icons.svg#bubbles2" /></svg>営業社員の登録</a>
          <ul id="dd02">
            <li><a href="<?php echo($this->Url->build(["controller" => "SalesPerformance", "action" => "index"])); ?>">
              <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#trophy" /></svg>営業実績</a></li>
            <li><a href="<?php echo($this->Url->build(["controller" => "SalesSkill", "action" => "index"])); ?>">
              <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#pencil" /></svg>営業資格一覧</a></li>
          </ul>
        </li>
        <li id="menu03"><a href="#dd03"  class="notLink"><svg role="image" class="icon"><use xlink:href="/svg/icons.svg#wrench" /></svg>技術社員の登録</a>
          <ul id="dd03">
            <li><a href="<?php echo($this->Url->build(["controller" => "TechniquePerformance", "action" => "index"])); ?>">
              <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#trophy" /></svg>技術実績</a></li>
            <li><a href="<?php echo($this->Url->build(["controller" => "TechniqueSkill", "action" => "index"])); ?>">
              <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#pencil" /></svg>技術資格一覧</a></li>
            <li><a href="<?php echo($this->Url->build(["controller" => "TechniqueSkillDb", "action" => "index"])); ?>">
              <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#database" /></svg>データベーススキル</a></li>
            <li><a href="<?php echo($this->Url->build(["controller" => "TechniqueSkillDesign", "action" => "index"])); ?>">
              <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#quill" /></svg>デザインスキル</a></li>
            <li><a href="<?php echo($this->Url->build(["controller" => "TechniqueSkillFramework", "action" => "index"])); ?>">
              <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#magic-wand" /></svg>フレームワークスキル</a></li>
            <li><a href="<?php echo($this->Url->build(["controller" => "TechniqueSkillLanguage", "action" => "index"])); ?>">
              <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#embed2" /></svg>プログラミングスキル</a></li>
            <li><a href="<?php echo($this->Url->build(["controller" => "techniqueSkillNetwork", "action" => "index"])); ?>">
              <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#sphere" /></svg>ネットワークスキル</a></li>
            <li><a href="<?php echo($this->Url->build(["controller" => "TechniqueSkillServer", "action" => "index"])); ?>">
              <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#terminal" /></svg>サーバースキル</a></li>
          </ul>
        </li>
      </ul>
    </nav>
  </div>

  <div class="rightNavi">
    <div class="rightContents">
      <h2><span>【閲覧】営業資格</span></h2>
      <div class="viewArea">
        <div class="viewContentsTypeA">
          <p>社員番号<span><?= h($salesSkill->number) ?></span></p>
          <p>運転免許<span><?= h($salesSkill->drivers_license) ?></span></p>
          <p>マイクロソフトオフィス<span><?= h($salesSkill->mos) ?></span></p>
          <p>ファイナンシャルプランナー<span><?= h($salesSkill->fp) ?></span></p>
          <p>宅建<span><?= h($salesSkill->taken) ?></span></p>
        </div>
        <div class="viewContentsTypeA">
          <p>販売士<span><?= h($salesSkill->sale) ?></span></p>
          <p>簿記<span><?= h($salesSkill->bookkeeping) ?></span></p>
          <p>基本情報技術者<span><?= h($salesSkill->basic) ?></span></p>
          <p>TOEIC<span><?= h($salesSkill->toeic) ?></span></p>
        </div>
        <div class="viewContentsTypeB">
          <h3>中国語</h3>
          <?= $this->Text->autoParagraph(h($salesSkill->chinese)); ?>
        </div>
        <div class="viewContentsTypeB">
          <h3>その他</h3>
          <?= $this->Text->autoParagraph(h($salesSkill->other)); ?>
        </div>
      </div>
    </div>
  </div>
</div>