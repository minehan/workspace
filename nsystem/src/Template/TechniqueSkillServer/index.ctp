<div class="wrapper">

  <div class="leftNavi">
    <nav>
      <h1 class="home02">
        <a href="<?php echo($this->Url->build(["controller" => "Users", "action" => "home"])); ?>">
          <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#logo" /></svg>NSYSTEM
        </a>
      </h1>
      <div class="leftContents">
        <h2>アクション</h2>
        <ul>
          <li><a href="<?php echo($this->Url->build(["controller" => "techniqueSkillServer", "action" => "add"])); ?>">
            <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#user-plus" /></svg>新規登録</a></li>
        </ul>
        <hr width="90%" align="right" color="#f60">
      </div>
      <ul id="dropDown">
        <li id="menu01"><a href="#dd01" class="notLink"><svg role="image" class="icon"><use xlink:href="/svg/icons.svg#office" /></svg>共通の登録</a>
          <ul id="dd01">
            <li><a href="<?php echo($this->Url->build(["controller" => "Personal", "action" => "index"])); ?>">
              <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#address-book" /></svg>個人情報</a></li>
            <li><a href="<?php echo($this->Url->build(["controller" => "Background", "action" => "index"])); ?>">
              <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#file-text2" /></svg>経歴</a></li>
          </ul>
        </li>
        <li id="menu02"><a href="#dd02"  class="notLink"><svg role="image" class="icon"><use xlink:href="/svg/icons.svg#bubbles2" /></svg>営業社員の登録</a>
          <ul id="dd02">
            <li><a href="<?php echo($this->Url->build(["controller" => "SalesPerformance", "action" => "index"])); ?>">
              <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#trophy" /></svg>営業実績</a></li>
            <li><a href="<?php echo($this->Url->build(["controller" => "SalesSkill", "action" => "index"])); ?>">
              <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#pencil" /></svg>営業資格一覧</a></li>
          </ul>
        </li>
        <li id="menu03"><a href="#dd03"  class="notLink"><svg role="image" class="icon"><use xlink:href="/svg/icons.svg#wrench" /></svg>技術社員の登録</a>
          <ul id="dd03">
            <li><a href="<?php echo($this->Url->build(["controller" => "TechniquePerformance", "action" => "index"])); ?>">
              <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#trophy" /></svg>技術実績</a></li>
            <li><a href="<?php echo($this->Url->build(["controller" => "TechniqueSkill", "action" => "index"])); ?>">
              <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#pencil" /></svg>技術資格一覧</a></li>
            <li><a href="<?php echo($this->Url->build(["controller" => "TechniqueSkillDb", "action" => "index"])); ?>">
              <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#database" /></svg>データベーススキル</a></li>
            <li><a href="<?php echo($this->Url->build(["controller" => "TechniqueSkillDesign", "action" => "index"])); ?>">
              <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#quill" /></svg>デザインスキル</a></li>
            <li><a href="<?php echo($this->Url->build(["controller" => "TechniqueSkillFramework", "action" => "index"])); ?>">
              <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#magic-wand" /></svg>フレームワークスキル</a></li>
            <li><a href="<?php echo($this->Url->build(["controller" => "TechniqueSkillLanguage", "action" => "index"])); ?>">
              <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#embed2" /></svg>プログラミングスキル</a></li>
            <li><a href="<?php echo($this->Url->build(["controller" => "techniqueSkillNetwork", "action" => "index"])); ?>">
              <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#sphere" /></svg>ネットワークスキル</a></li>
            <li><a href="<?php echo($this->Url->build(["controller" => "TechniqueSkillServer", "action" => "index"])); ?>">
              <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#terminal" /></svg>サーバースキル</a></li>
          </ul>
        </li>
      </ul>
    </nav>
  </div>

  <div class="rightNavi">
    <div class="rightContents">
      <h2><span>サーバースキル</span></h2>
      <div class="directions">
        <p>※表示の情報は厳選しています。詳しい内容は閲覧をご覧ください</p>
      </div>
      <div class="table">
        <div class="header">
          <div class="cell">
            <?= $this->Paginator->sort('number', '<svg role="image" class="icon"><use xlink:href="/svg/icons.svg#user" /></svg>社員番号<br>', array('escape' => false)) ?>
          </div>
          <div class="cell">
            <?= $this->Paginator->sort('iis', '<svg role="image" class="icon"><use xlink:href="/svg/icons.svg#terminal" /></svg>IIS<br>', array('escape' => false)) ?>
          </div>
          <div class="cell">
            <?= $this->Paginator->sort('apache', '<svg role="image" class="icon"><use xlink:href="/svg/icons.svg#terminal" /></svg>アパッチ<br>', array('escape' => false)) ?>
          </div>
          <div class="cell">
            <?= $this->Paginator->sort('lighttpd', '<svg role="image" class="icon"><use xlink:href="/svg/icons.svg#terminal" /></svg>ライティ<br>', array('escape' => false)) ?>
          </div>
          <div class="cell">
            <?= $this->Paginator->sort('nignx', '<svg role="image" class="icon"><use xlink:href="/svg/icons.svg#terminal" /></svg>エンジンエックス<br>', array('escape' => false)) ?>
          </div>
          <div class="cell">
            <?= $this->Paginator->sort('tomcat', '<svg role="image" class="icon"><use xlink:href="/svg/icons.svg#terminal" /></svg>トムキャット<br>', array('escape' => false)) ?>
          </div>
          <div class="cell">
            <?= $this->Paginator->sort('postfix', '<svg role="image" class="icon"><use xlink:href="/svg/icons.svg#terminal" /></svg>ポストフィックス<br>', array('escape' => false)) ?>
          </div>
          <div class="cell">
            <?= $this->Paginator->sort('sendmail', '<svg role="image" class="icon"><use xlink:href="/svg/icons.svg#terminal" /></svg>センドメール<br>', array('escape' => false)) ?>
          </div>
          <div class="cell">
            <p>アクション</p>
          </div>
        </div>
        <?php foreach ($techniqueSkillServer as $techniqueSkillServer): ?>
        <div class="row">
          <div class="cell">
            <?= h($techniqueSkillServer->number) ?>
          </div>
          <div class="cell">
            <?= h($techniqueSkillServer->iis) ?>
          </div>
          <div class="cell">
            <?= h($techniqueSkillServer->apache) ?>
          </div>
          <div class="cell">
            <?= h($techniqueSkillServer->lighttpd) ?>
          </div>
          <div class="cell">
            <?= h($techniqueSkillServer->nignx) ?>
          </div>
          <div class="cell">
            <?= h($techniqueSkillServer->tomcat) ?>
          </div>
          <div class="cell">
            <?= h($techniqueSkillServer->postfix) ?>
          </div>
          <div class="cell">
            <?= h($techniqueSkillServer->sendmail) ?>
          </div>
          <div class="cell">
            <a href="<?php echo($this->Url->build(["controller" => "techniqueSkillServer", "action" => "view", $techniqueSkillServer->id])); ?>"><svg role="image" class="icon"><use xlink:href="/svg/icons.svg#eye" /></svg>閲覧</a>
            <a href="<?php echo($this->Url->build(["controller" => "techniqueSkillServer", "action" => "edit", $techniqueSkillServer->id])); ?>"><svg role="image" class="icon"><use xlink:href="/svg/icons.svg#pencil2" /></svg>編集</a>
            <?= $this->Form->postLink('<svg role="image" class="icon"><use xlink:href="/svg/icons.svg#user-minus" /></svg>削除', ['action' => 'delete', $techniqueSkillServer->id], ['escape' => false, 'confirm' => __('本当に削除してもよろしいですか?', $techniqueSkillServer->id)]) ?>
          </div>
        </div>
        <?php endforeach; ?>
      </div>
      <div class="paginator">
        <ul>
          <?= $this->Paginator->prev('<svg role="image" class="icon"><use xlink:href="/svg/icons.svg#arrow-left" /></svg>前', array('escape' => false)) ?>
          <?= $this->Paginator->numbers() ?>
          <?= $this->Paginator->next('次<svg role="image" class="icon"><use xlink:href="/svg/icons.svg#arrow-right" /></svg>', array('escape' => false)) ?>
        </ul>
      </div>
    </div>
  </div>
</div>