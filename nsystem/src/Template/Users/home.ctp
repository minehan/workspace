<div class="wrapper">
  <div class="leftNavi">
    <nav>
      <h1 class="home02">
        <a href="<?php echo($this->Url->build(["controller" => "Users", "action" => "home"])); ?>">
          <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#logo" /></svg>NSYSTEM
        </a>
      </h1>
      <div class="searchBox">
        <?= $this->Form->create() ?>
        <fieldset>
         <p>社員番号を入力</p>
          <?php echo $this->Form->input('find',[
        'label'=>[
          'text'=>false
        ],
        'required'=>'required'
      ]);
          ?>
          <button type="submit"><svg role="image" class="iconSearch"><use xlink:href="/svg/icons.svg#search" /></svg></button>
        </fieldset>
        <?= $this->Form->end() ?>
      </div>
      <!--dropDown内部インデントをずらすとサイドナビが表示されない cakeかscssのバグ？-->
      <ul id="dropDown">
        <li id="menu01"><a href="#dd01" class="notLink"><svg role="image" class="icon"><use xlink:href="/svg/icons.svg#office" /></svg>共通の登録</a>
          <ul id="dd01">
            <li><a href="<?php echo($this->Url->build(["controller" => "Personal", "action" => "index"])); ?>">
              <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#address-book" /></svg>個人情報</a></li>
            <li><a href="<?php echo($this->Url->build(["controller" => "Background", "action" => "index"])); ?>">
            <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#file-text2" /></svg>経歴</a></li>
          </ul>
        </li>
        <li id="menu02"><a href="#dd02"  class="notLink"><svg role="image" class="icon"><use xlink:href="/svg/icons.svg#bubbles2" /></svg>営業社員の登録</a>
          <ul id="dd02">
            <li><a href="<?php echo($this->Url->build(["controller" => "SalesPerformance", "action" => "index"])); ?>">
            <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#trophy" /></svg>営業実績</a></li>
            <li><a href="<?php echo($this->Url->build(["controller" => "SalesSkill", "action" => "index"])); ?>">
            <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#pencil" /></svg>営業資格一覧</a></li>
          </ul>
        </li>
        <li id="menu03"><a href="#dd03"  class="notLink"><svg role="image" class="icon"><use xlink:href="/svg/icons.svg#wrench" /></svg>技術社員の登録</a>
          <ul id="dd03">
            <li><a href="<?php echo($this->Url->build(["controller" => "TechniquePerformance", "action" => "index"])); ?>">
            <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#trophy" /></svg>技術実績</a></li>
            <li><a href="<?php echo($this->Url->build(["controller" => "TechniqueSkill", "action" => "index"])); ?>">
            <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#pencil" /></svg>技術資格一覧</a></li>
            <li><a href="<?php echo($this->Url->build(["controller" => "TechniqueSkillDb", "action" => "index"])); ?>">
            <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#database" /></svg>データベーススキル</a></li>
            <li><a href="<?php echo($this->Url->build(["controller" => "TechniqueSkillDesign", "action" => "index"])); ?>">
            <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#quill" /></svg>デザインスキル</a></li>
            <li><a href="<?php echo($this->Url->build(["controller" => "TechniqueSkillFramework", "action" => "index"])); ?>">
            <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#magic-wand" /></svg>フレームワークスキル</a></li>
            <li><a href="<?php echo($this->Url->build(["controller" => "TechniqueSkillLanguage", "action" => "index"])); ?>">
            <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#embed2" /></svg>プログラミングスキル</a></li>
            <li><a href="<?php echo($this->Url->build(["controller" => "techniqueSkillNetwork", "action" => "index"])); ?>">
            <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#sphere" /></svg>ネットワークスキル</a></li>
            <li><a href="<?php echo($this->Url->build(["controller" => "TechniqueSkillServer", "action" => "index"])); ?>">
            <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#terminal" /></svg>サーバースキル</a></li>
          </ul>
        </li>
      </ul>
    </nav>
  </div>

  <div class="rightNavi">
        <?php $i = 0; ?>
        <?php foreach ($personal as $personal): if($i < 1): ?>
    <div class="table">
      <div class="header">
        <div class="cell">
          該当レコード
        </div>
        <div class="cell">
          件数
        </div>
      </div>
      <div class="row">
        <div class="cell">
          <a href="<?php echo($this->Url->build(["controller" => "Personal", "action" => "index"])); ?>">
            <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#user" /></svg>個人情報
          </a>
        </div>
        <div class="cell">
          <?= h($personalCount) ?>
        </div>
      </div>
        <?php ++$i; ?>
        <?php endif; ?>
        <?php endforeach; ?>
      <?php $i = 0; ?>
      <?php foreach ($background as $background): if($i < 1): ?>
      <div class="row">
        <div class="cell">
          <a href="<?php echo($this->Url->build(["controller" => "Background", "action" => "index"])); ?>">
            <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#file-text2" /></svg>経歴
          </a>
        </div>
        <div class="cell">
          <?= h($backgroundCount) ?>
        </div>
      </div>
      <?php ++$i; ?>
      <?php endif; ?>
      <?php endforeach; ?>
      <?php $i = 0; ?>
      <?php foreach ($salesPerformance as $salesPerformance): if($i < 1): ?>
      <div class="row">
        <div class="cell">
          <a href="<?php echo($this->Url->build(["controller" => "SalesPerformance", "action" => "index"])); ?>">
            <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#trophy" /></svg>営業実績
          </a>
        </div>
        <div class="cell">
          <?= h($salesPerformanceCount) ?>
        </div>
      </div>
      <?php ++$i; ?>
      <?php endif; ?>
      <?php endforeach; ?>
      <?php $i = 0; ?>
      <?php foreach ($salesSkill as $salesSkill): if($i < 1): ?>
      <div class="row">
        <div class="cell">
          <a href="<?php echo($this->Url->build(["controller" => "SalesSkill", "action" => "index"])); ?>">
            <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#pencil" /></svg>営業資格一覧
          </a>
        </div>
        <div class="cell">
          <?= h($salesSkillCount) ?>
        </div>
      </div>
      <?php ++$i; ?>
      <?php endif; ?>
      <?php endforeach; ?>
      <?php $i = 0; ?>
      <?php foreach ($techniquePerformance as $techniquePerformance): if($i < 1): ?>
      <div class="row">
        <div class="cell">
          <a href="<?php echo($this->Url->build(["controller" => "TechniquePerformance", "action" => "index"])); ?>">
            <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#trophy" /></svg>技術実績
          </a>
        </div>
        <div class="cell">
          <?= h($techniquePerformanceCount) ?>
        </div>
      </div>
      <?php ++$i; ?>
      <?php endif; ?>
      <?php endforeach; ?>
      <?php $i = 0; ?>
      <?php foreach ($techniqueSkill as $techniqueSkill): if($i < 1): ?>
      <div class="row">
        <div class="cell">
          <a href="<?php echo($this->Url->build(["controller" => "TechniqueSkill", "action" => "index"])); ?>">
            <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#pencil" /></svg>技術資格一覧
          </a>
        </div>
        <div class="cell">
          <?= h($techniqueSkillCount) ?>
        </div>
      </div>
      <?php ++$i; ?>
      <?php endif; ?>
      <?php endforeach; ?>
      <?php $i = 0; ?>
      <?php foreach ($techniqueSkillDb as $techniqueSkillDb): if($i < 1): ?>
      <div class="row">
        <div class="cell">
          <a href="<?php echo($this->Url->build(["controller" => "TechniqueSkillDb", "action" => "index"])); ?>">
            <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#database" /></svg>データベーススキル
          </a>
        </div>
        <div class="cell">
          <?= h($techniqueSkillDbCount) ?>
        </div>
      </div>
      <?php ++$i; ?>
      <?php endif; ?>
      <?php endforeach; ?>
      <?php $i = 0; ?>
      <?php foreach ($techniqueSkillDesign as $techniqueSkillDesign): if($i < 1): ?>
      <div class="row">
        <div class="cell">
          <a href="<?php echo($this->Url->build(["controller" => "TechniqueSkillDesign", "action" => "index"])); ?>">
            <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#quill" /></svg>デザインスキル
          </a>
        </div>
        <div class="cell">
          <?= h($techniqueSkillDesignCount) ?>
        </div>
      </div>
      <?php ++$i; ?>
      <?php endif; ?>
      <?php endforeach; ?>
      <?php $i = 0; ?>
      <?php foreach ($techniqueSkillFramework as $techniqueSkillFramework): if($i < 1): ?>
      <div class="row">
        <div class="cell">
          <a href="<?php echo($this->Url->build(["controller" => "TechniqueSkillFramework", "action" => "index"])); ?>">
            <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#magic-wand" /></svg>フレームワークスキル
          </a>
        </div>
        <div class="cell">
          <?= h($techniqueSkillFrameworkCount) ?>
        </div>
      </div>
      <?php ++$i; ?>
      <?php endif; ?>
      <?php endforeach; ?>
      <?php $i = 0; ?>
      <?php foreach ($techniqueSkillLanguage as $techniqueSkillLanguage): if($i < 1): ?>
      <div class="row">
        <div class="cell">
          <a href="<?php echo($this->Url->build(["controller" => "TechniqueSkillLanguage", "action" => "index"])); ?>">
            <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#embed2" /></svg>プログラミングスキル
          </a>
        </div>
        <div class="cell">
          <?= h($techniqueSkillLanguageCount) ?>
        </div>
      </div>
      <?php ++$i; ?>
      <?php endif; ?>
      <?php endforeach; ?>
      <?php $i = 0; ?>
      <?php foreach ($techniqueSkillNetwork as $techniqueSkillNetwork): if($i < 1): ?>
      <div class="row">
        <div class="cell">
          <a href="<?php echo($this->Url->build(["controller" => "TechniqueSkillNetwork", "action" => "index"])); ?>">
            <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#sphere" /></svg>ネットワークスキル
          </a>
        </div>
        <div class="cell">
          <?= h($techniqueSkillNetworkCount) ?>
        </div>
      </div>
      <?php ++$i; ?>
      <?php endif; ?>
      <?php endforeach; ?>
      <?php $i = 0; ?>
      <?php foreach ($techniqueSkillServer as $techniqueSkillServer): if($i < 1): ?>
      <div class="row">
        <div class="cell">
          <a href="<?php echo($this->Url->build(["controller" => "TechniqueSkillServer", "action" => "index"])); ?>">
            <svg role="image" class="icon"><use xlink:href="/svg/icons.svg#terminal" /></svg>サーバースキル
          </a>
        </div>
        <div class="cell">
          <?= h($techniqueSkillServerCount) ?>
        </div>
      </div>
      <?php ++$i; ?>
      <?php endif; ?>
      <?php endforeach; ?>
    </div>
  </div>
</div>



