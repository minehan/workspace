<div class="loginContens">
  <figure><?php echo $this->Html->image('logo.png',[
  'alt' => 'logo',
  'width' => '84px',
  'height' => '84px',
  ]); ?></figure>
<!--  <svg role="image" class="icon">
    <use xlink:href="/svg/icons.svg#Download" />
  </svg>-->
<div class="loginForm">
<?= $this->Form->create() ?>
<!--//テキストとテキストボックスの横並べは
'label'=>[text=>hogehoge]-->
<?= $this->Form->input('username',[
  'label'=> [
    'text'=>'管理者名'
  ]
]) ?>
<?= $this->Form->input('password',[
  'label'=> [
    'text'=>'パスワード'
  ]
]) ?>
  <button type="submit"><svg role="image" class="icon"><use xlink:href="/svg/icons.svg#enter" /></svg>ログイン</button>
<?= $this->Form->end() ?>
</div>
</div>
