<div class="wrapper">
  <video id="video" autoplay loop muted>
    <source src="<?= $this->Url->build('/video/IV.mp4') ?>" type="video/webm">
  </video>
</div>

<div class="titleArea">
  <a href="<?= $this->Url->build(["controller" => "Users", "action" => "home"])?>">
    <ul>
      <li><span>管理システム</span></li>
      <li>社員データの登録&amp;更新業務はこちら</li>
    </ul>
  </a>
  <a href="">
    <ul>
      <li><span>閲覧ページ</span></li>
      <li>社員データの閲覧&amp;プロジェクト管理はこちら</li>
    </ul>
  </a>
</div>

<?= $this->Html->script('video.js') ?>