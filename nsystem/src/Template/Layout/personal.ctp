<!DOCTYPE html>
<html>
  <head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>nsystem</title>
    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('reset.css') ?>
    <?= $this->Html->css('style.css') ?>
    <?= $this->Html->script('jquery-1.12.4.min.js') ?>
    <?= $this->Html->script('main.js') ?>
  </head>

  <body>
    <nav class="top-bar expanded" data-topbar role="navigation">
      <ul class="title-area large-3 medium-4 columns">
        <li class="name">
          <h1><a href="#">社員登録ページ（仮）</a></h1>
        </li>
      </ul>
      <div class="top-bar-section">
        <ul class="right">
          <li><a href="http://localhost:8888/nkadai/users">管理者ページ</a></li>
          <li><a href="http://localhost:8888/nkadai/users/logout">ログアウト</a></li>
        </ul>
      </div>
    </nav>
    <?= $this->Flash->render() ?>
    <div class="container clearfix">
      <?= $this->fetch('content') ?>
    </div>
    <footer>
    </footer>
  </body>
</html>