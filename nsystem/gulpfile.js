/************************************************
* モジュールの読み込みや設定
************************************************/

var gulp = require('gulp');
var path = ('path'); //node.js api
var fs = require('fs'); //node.js api
var $ = require('gulp-load-plugins')(); //プラグインがgulp-××-××のタイプは記述できない。暇があったら修正
var sftpconfig = JSON.parse(fs.readFileSync('sftpconfig.json', 'utf8')); //サーバー情報を外部ファイルから読み込み
var runSequence = require('run-sequence');
var minifycss = require('gulp-minify-css');
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;

//プロキシサーバを立ち上げるための設定
var conf = {
  proxy: {
    proxy: 'http://cake.minehan.xyz/',
    port: 1234,
    open: false,
    notify: false
  }
};

/************************************************
* タスク一覧
************************************************/

//scssのコンパイル※compass使用+css整形+minify
gulp.task('compass', function(){
    gulp.src('webroot/scss/*.scss')
    .pipe($.plumber())
    .pipe($.compass({
      config_file: 'webroot/config.rb',
      comments: false,
      css: 'webroot/css/',
      sass: 'webroot/scss/'
    }))
    .pipe($.csscomb())
    .pipe($.autoprefixer())
    .pipe(gulp.dest('webroot/css'))
    .pipe(minifycss())
    .pipe($.rename({
      extname: '-min.css'
    }))
    .pipe(gulp.dest('webroot/css'));
});

gulp.task('svg', function () {
  return gulp.src('webroot/svg/icons/*.svg')
    .pipe($.svgmin())
    .pipe($.svgstore({ inlineSvg: true }))
    .pipe($.cheerio({
    run: function ($, file) {
      $('svg').addClass('hide');
      $('[fill]').removeAttr('fill');
    },
    parserOptions: { xmlMode: true }
  }))
    .pipe(gulp.dest('webroot/svg'));
});
gulp.task('svg2png', function () {
  return gulp.src('webroot/svg/icons/*.svg')
    .pipe($.svg2png())
    .pipe($.rename({ prefix: "icons.svg." }))
    .pipe($.imagemin())
    .pipe(gulp.dest('webroot/svg'));
});

gulp.task('svgSprite', function(callback){
  runSequence('svg', 'svg2png', callback);
});

//画像の圧縮
gulp.task('imageMin', function(){
  gulp.src(['webroot/material/*.png', 'webroot/material/*.gif'])
    .pipe($.imagemin())
    .pipe(gulp.dest('webroot/img/'));
});

//browserSyncのビルド
gulp.task('server', function() {
  browserSync.init(conf.proxy);
});

//upload
gulp.task('uploadImg', function () {
    var server = sftpconfig.server[0];
    var path = sftpconfig.path[0];
    Object.assign(server, path);
    gulp.src([
      'webroot/img/*.png',
      'webroot/img/*.jpg',
      'webroot/img/*.gif'
    ])
      .pipe($.plumber())
      .pipe($.sftp(server))
      .pipe(gulp.dest('garbageBox/'));
});
gulp.task('uploadJs', function () {
    var server = sftpconfig.server[0];
    var path = sftpconfig.path[1];
    Object.assign(server, path);
    gulp.src([
      'webroot/js/*.js'
    ])
      .pipe($.plumber())
      .pipe($.sftp(server))
      .pipe(gulp.dest('garbageBox/'));
});
gulp.task('uploadCss', function () {
    var server = sftpconfig.server[0];
    var path = sftpconfig.path[2];
    Object.assign(server, path);
    gulp.src([
      'webroot/css/*.css',
      '!webroot/css/style-index.css',
      '!webroot/css/style-backend.css',
      '!webroot/css/style-frontend.css'
    ])
      .pipe($.plumber())
      .pipe($.sftp(server))
      .pipe(gulp.dest('garbageBox/'));
});
gulp.task('uploadSvg', function () {
    var server = sftpconfig.server[0];
    var path = sftpconfig.path[3];
    Object.assign(server, path);
    gulp.src([
      'webroot/svg/*.svg',
      'webroot/svg/*.png'
    ])
      .pipe($.plumber())
      .pipe($.sftp(server))
      .pipe(gulp.dest('garbageBox/'));
});
gulp.task('uploadController', function () {
    var server = sftpconfig.server[0];
    var path = sftpconfig.path[4];
    Object.assign(server, path);
    gulp.src([
      'src/Controller/*.php'
    ])
      .pipe($.plumber())
      .pipe($.sftp(server))
      .pipe(gulp.dest('garbageBox/'));
});
gulp.task('uploadTable', function () {
    var server = sftpconfig.server[0];
    var path = sftpconfig.path[5];
    Object.assign(server, path);
    gulp.src([
      'src/Model/Table/*.php'
    ])
      .pipe($.plumber())
      .pipe($.sftp(server))
      .pipe(gulp.dest('garbageBox/'));
});
gulp.task('uploadEntity', function () {
    var server = sftpconfig.server[0];
    var path = sftpconfig.path[6];
    Object.assign(server, path);
    gulp.src([
      'src/Model/Entity/*.php'
    ])
      .pipe($.plumber())
      .pipe($.sftp(server))
      .pipe(gulp.dest('garbageBox/'));
});
gulp.task('uploadBackground', function () {
    var server = sftpconfig.server[0];
    var path = sftpconfig.path[7];
    Object.assign(server, path);
    gulp.src([
      'src/Template/Background/*.ctp'
    ])
      .pipe($.plumber())
      .pipe($.sftp(server))
      .pipe(gulp.dest('garbageBox/'));
});
gulp.task('uploadLayout', function () {
    var server = sftpconfig.server[0];
    var path = sftpconfig.path[8];
    Object.assign(server, path);
    gulp.src([
      'src/Template/Layout/*.ctp'
    ])
      .pipe($.plumber())
      .pipe($.sftp(server))
      .pipe(gulp.dest('garbageBox/'));
});
gulp.task('uploadPersonal', function () {
    var server = sftpconfig.server[0];
    var path = sftpconfig.path[9];
    Object.assign(server, path);
    gulp.src([
      'src/Template/Personal/*.ctp'
    ])
      .pipe($.plumber())
      .pipe($.sftp(server))
      .pipe(gulp.dest('garbageBox/'));
});
gulp.task('uploadSalesPerformance', function () {
    var server = sftpconfig.server[0];
    var path = sftpconfig.path[10];
    Object.assign(server, path);
    gulp.src([
      'src/Template/SalesPerformance/*.ctp'
    ])
      .pipe($.plumber())
      .pipe($.sftp(server))
      .pipe(gulp.dest('garbageBox/'));
});
gulp.task('uploadSalesSkill', function () {
    var server = sftpconfig.server[0];
    var path = sftpconfig.path[11];
    Object.assign(server, path);
    gulp.src([
      'src/Template/SalesSkill/*.ctp'
    ])
      .pipe($.plumber())
      .pipe($.sftp(server))
      .pipe(gulp.dest('garbageBox/'));
});
gulp.task('uploadTechniquePerformance', function () {
    var server = sftpconfig.server[0];
    var path = sftpconfig.path[12];
    Object.assign(server, path);
    gulp.src([
      'src/Template/TechniquePerformance/*.ctp'
    ])
      .pipe($.plumber())
      .pipe($.sftp(server))
      .pipe(gulp.dest('garbageBox/'));
});
gulp.task('uploadTechniqueSkill', function () {
    var server = sftpconfig.server[0];
    var path = sftpconfig.path[13];
    Object.assign(server, path);
    return gulp.src([
      'src/Template/TechniqueSkill/*.ctp'
    ])
      .pipe($.plumber())
      .pipe($.sftp(server))
      .pipe(gulp.dest('garbageBox/'));
});
gulp.task('uploadTechniqueSkillDb', function () {
    var server = sftpconfig.server[0];
    var path = sftpconfig.path[14];
    Object.assign(server, path);
    gulp.src([
      'src/Template/TechniqueSkillDb/*.ctp'
    ])
      .pipe($.plumber())
      .pipe($.sftp(server))
      .pipe(gulp.dest('garbageBox/'));
});
gulp.task('uploadTechniqueSkillDesign', function () {
    var server = sftpconfig.server[0];
    var path = sftpconfig.path[15];
    Object.assign(server, path);
    gulp.src([
      'src/Template/TechniqueSkillDesign/*.ctp'
    ])
      .pipe($.plumber())
      .pipe($.sftp(server))
      .pipe(gulp.dest('garbageBox/'));
});
gulp.task('uploadTechniqueSkillFramework', function () {
    var server = sftpconfig.server[0];
    var path = sftpconfig.path[16];
    Object.assign(server, path);
    return gulp.src([
      'src/Template/TechniqueSkillFramework/*.ctp'
    ])
      .pipe($.plumber())
      .pipe($.sftp(server))
      .pipe(gulp.dest('garbageBox/'));
});
gulp.task('uploadTechniqueSkillLanguage', function () {
    var server = sftpconfig.server[0];
    var path = sftpconfig.path[17];
    Object.assign(server, path);
    gulp.src([
      'src/Template/TechniqueSkillLanguage/*.ctp'
    ])
      .pipe($.plumber())
      .pipe($.sftp(server))
      .pipe(gulp.dest('garbageBox/'));
});
gulp.task('uploadTechniqueSkillNetwork', function () {
    var server = sftpconfig.server[0];
    var path = sftpconfig.path[18];
    Object.assign(server, path);
    gulp.src([
      'src/Template/TechniqueSkillNetwork/*.ctp'
    ])
      .pipe($.plumber())
      .pipe($.sftp(server))
      .pipe(gulp.dest('garbageBox/'));
});
gulp.task('uploadTechniqueSkillServer', function () {
    var server = sftpconfig.server[0];
    var path = sftpconfig.path[19];
    Object.assign(server, path);
    gulp.src([
      'src/Template/TechniqueSkillServer/*.ctp'
    ])
      .pipe($.plumber())
      .pipe($.sftp(server))
      .pipe(gulp.dest('garbageBox/'));
});
gulp.task('uploadUsers', function () {
    var server = sftpconfig.server[0];
    var path = sftpconfig.path[20];
    Object.assign(server, path);
    gulp.src([
      'src/Template/Users/*.ctp'
    ])
      .pipe($.plumber())
      .pipe($.sftp(server))
      .pipe(gulp.dest('garbageBox/'));
});
gulp.task('uploadValidation', function () {
  var server = sftpconfig.server[0];
  var path = sftpconfig.path[21];
  Object.assign(server, path);
  gulp.src([
    'src/Validation/*.php'
  ])
    .pipe($.plumber())
    .pipe($.sftp(server))
    .pipe(gulp.dest('garbageBox/'));
});

gulp.task('watch', function(){
  gulp.watch('webroot/scss/*.scss',['compass']);
  gulp.watch('webroot/img/**',['uploadImg']);
  gulp.watch('webroot/js/**',['uploadJs']);
  gulp.watch('webroot/css/**',['uploadCss']);
  gulp.watch('webroot/svg/**',['uploadSvg']);
  gulp.watch('src/Controller/**',['uploadController']);
  gulp.watch('src/Model/Table/**',['uploadTable']);
  gulp.watch('src/Model/Entity/**',['uploadEntity']);
  gulp.watch('src/Template/Background/**',['uploadBackground']);
  gulp.watch('src/Template/Layout/**',['uploadLayout']);
  gulp.watch('src/Template/Personal/**',['uploadPersonal']);
  gulp.watch('src/Template/SalesPerformance/**',['uploadSalesPerformance']);
  gulp.watch('src/Template/SalesSkill/**',['uploadSalesSkill']);
  gulp.watch('src/Template/TechniquePerformance/**',['uploadTechniquePerformance']);
  gulp.watch('src/Template/TechniqueSkill/**',['uploadTechniqueSkill']);
  gulp.watch('src/Template/TechniqueSkillDb/**',['uploadTechniqueSkillDb']);
  gulp.watch('src/Template/TechniqueSkillDesign/**',['uploadTechniqueSkillDesign']);
  gulp.watch('src/Template/TechniqueSkillFramework/**',['uploadTechniqueSkillFramework']);
  gulp.watch('src/Template/TechniqueSkillLanguage/**',['uploadTechniqueSkillLanguage']);
  gulp.watch('src/Template/TechniqueSkillNetwork/**',['uploadTechniqueSkillNetwork']);
  gulp.watch('src/Template/TechniqueSkillServer/**',['uploadTechniqueSkillServer']);
  gulp.watch('src/Template/Users/**',['uploadUsers']);
  gulp.watch('src/Validation/**',['uploadValidation']);
  gulp.watch('garbageBox/**', reload);
});

gulp.task('default', ['server'], function(){
  gulp.start(['watch']);
});

