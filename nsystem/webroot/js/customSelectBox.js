/*
if(typeof jQuery != "undefined"){ //jQueryの読み込み確認
  $(function(){
    alert('jQuery読み込みテストです')
  });
}
*/

//Personal add
StartdayYear = function(select, obj){
  var set_selectbox01 = function(){
    var startyear = $('select[name="startday[year]"] option:selected').text();
    $(select).find(obj).find('.year01').html(startyear);
  }
  $(select).find('select').each(set_selectbox01).on('change', set_selectbox01);
}

StartdayMonth = function(select, obj){
  var set_selectbox02 = function(){
    var startmonth = $('select[name="startday[month]"] option:selected').text();
    $(select).find(obj).find('.month01').html(startmonth);
  }
  $(select).find('select').each(set_selectbox02).on('change', set_selectbox02);
}

StartdayDay = function(select, obj){
  var set_selectbox03 = function(){
    var startdayday = $('select[name="startday[day]"] option:selected').text();
    $(select).find(obj).find('.day01').html(startdayday);
  }
  $(select).find('select').each(set_selectbox03).on('change', set_selectbox03);
}

$(function(){
  StartdayYear('.customSelectbox', '.inner');
  StartdayMonth('.customSelectbox', '.inner');
  StartdayDay('.customSelectbox', '.inner');
});

//SalesPerformance add 
MonthYear = function(select, obj){
  var set_selectbox04 = function(){
    var monthyear = $('select[name="month[year]"] option:selected').text();
    $(select).find(obj).find('.year02').html(monthyear);
  }
  $(select).find('select').each(set_selectbox04).on('change', set_selectbox04);
}

MonthMonth = function(select, obj){
  var set_selectbox05 = function(){
    var monthmonth = $('select[name="month[month]"] option:selected').text();
    $(select).find(obj).find('.month02').html(monthmonth);
  }
  $(select).find('select').each(set_selectbox05).on('change', set_selectbox05);
}

$(function(){
  MonthYear('.customSelectbox', '.inner');
  MonthMonth('.customSelectbox', '.inner');
});
