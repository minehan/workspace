//サイドグローバルナビ
var dropdown = function(e) {
  var node       = null,
      anchorNode = null,
      targetId   = '',
      thisId     = '',
      nodes      = this.parentNode.childNodes;

  anchorNode = this.childNodes[0];
  targetId   = anchorNode.href;
  if ((n = targetId.lastIndexOf('#')) !== -1) {
    thisId = targetId.substring(n).replace('#', '');
    node   = document.getElementById(thisId);
    if (e.type === 'mouseover') {
      node.style.display    = 'block';
      node.style.visibility = 'visible';
    } else {
      node.style.display    = 'none';
      node.style.visibility = 'hidden';
    }
  }

  for (var i = 0, len = nodes.length; i < len; i++) {
    if (nodes[i].nodeType === 1) {
      anchorNode = nodes[i].childNodes[0];
      targetId   = anchorNode.href;
      if ((n = targetId.lastIndexOf('#')) !== -1) {
        targetId = targetId.substring(n).replace('#', '');
        if (targetId !== '' && thisId !== targetId) {
          node = document.getElementById(targetId);
          node.style.display    = 'none';
          node.style.visibility = 'hidden';
        }
      }
    }
  }
};

var menu01Btn = document.getElementById('menu01');
var menu02Btn = document.getElementById('menu02');
var menu03Btn = document.getElementById('menu03');

menu01Btn.onmouseover = dropdown;
menu01Btn.onmouseout  = dropdown;
menu02Btn.onmouseover = dropdown;
menu02Btn.onmouseout  = dropdown;
menu03Btn.onmouseover = dropdown;
menu03Btn.onmouseout  = dropdown;