$(function () {
  var monitarHeight = $(window).outerHeight();
  var getWindowMovieHeight = function (vw, vh) {
    var windowSizeHeight = $(window).outerHeight();
    var windowSizeWidth = $(window).outerWidth();

    var windowMovieSizeWidth = windowSizeHeight * vw;
    var windowMovieSizeHeight = windowSizeWidth / vh;
    var windowMovieSizeWidthLeftMargin = (windowMovieSizeWidth - windowSizeWidth) / 2;

    if (windowMovieSizeHeight < windowSizeHeight) {
      $("#video").css({
        left: -windowMovieSizeWidthLeftMargin
      });
    }
  };

  $(window).on('load', function () {
    getWindowMovieHeight(1.95, 1.95);
    if (monitarHeight < 800) {
      getWindowMovieHeight(2.85, 2.85);
    }
  });

  $(window).on('resize', function () {
    getWindowMovieHeight(1.95, 1.95);
    if (monitarHeight < 800) {
      getWindowMovieHeight(2.85, 2.85);
    }
  });

});