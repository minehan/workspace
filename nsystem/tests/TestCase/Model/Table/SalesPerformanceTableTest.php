<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SalesPerformanceTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SalesPerformanceTable Test Case
 */
class SalesPerformanceTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SalesPerformanceTable
     */
    public $SalesPerformance;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.sales_performance'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SalesPerformance') ? [] : ['className' => 'App\Model\Table\SalesPerformanceTable'];
        $this->SalesPerformance = TableRegistry::get('SalesPerformance', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SalesPerformance);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
