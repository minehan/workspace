<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TechniqueSkillDesignTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TechniqueSkillDesignTable Test Case
 */
class TechniqueSkillDesignTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TechniqueSkillDesignTable
     */
    public $TechniqueSkillDesign;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.technique_skill_design'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TechniqueSkillDesign') ? [] : ['className' => 'App\Model\Table\TechniqueSkillDesignTable'];
        $this->TechniqueSkillDesign = TableRegistry::get('TechniqueSkillDesign', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TechniqueSkillDesign);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
