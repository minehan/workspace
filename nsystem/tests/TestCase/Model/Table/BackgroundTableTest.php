<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BackgroundTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BackgroundTable Test Case
 */
class BackgroundTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\BackgroundTable
     */
    public $Background;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.background'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Background') ? [] : ['className' => 'App\Model\Table\BackgroundTable'];
        $this->Background = TableRegistry::get('Background', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Background);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
