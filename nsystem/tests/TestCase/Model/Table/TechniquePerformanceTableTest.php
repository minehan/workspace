<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TechniquePerformanceTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TechniquePerformanceTable Test Case
 */
class TechniquePerformanceTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TechniquePerformanceTable
     */
    public $TechniquePerformance;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.technique_performance'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TechniquePerformance') ? [] : ['className' => 'App\Model\Table\TechniquePerformanceTable'];
        $this->TechniquePerformance = TableRegistry::get('TechniquePerformance', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TechniquePerformance);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
