<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TechniqueSkillLanguageTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TechniqueSkillLanguageTable Test Case
 */
class TechniqueSkillLanguageTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TechniqueSkillLanguageTable
     */
    public $TechniqueSkillLanguage;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.technique_skill_language'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TechniqueSkillLanguage') ? [] : ['className' => 'App\Model\Table\TechniqueSkillLanguageTable'];
        $this->TechniqueSkillLanguage = TableRegistry::get('TechniqueSkillLanguage', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TechniqueSkillLanguage);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
