<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TechniqueSkillServerTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TechniqueSkillServerTable Test Case
 */
class TechniqueSkillServerTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TechniqueSkillServerTable
     */
    public $TechniqueSkillServer;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.technique_skill_server'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TechniqueSkillServer') ? [] : ['className' => 'App\Model\Table\TechniqueSkillServerTable'];
        $this->TechniqueSkillServer = TableRegistry::get('TechniqueSkillServer', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TechniqueSkillServer);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
