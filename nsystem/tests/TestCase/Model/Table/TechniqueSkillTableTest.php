<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TechniqueSkillTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TechniqueSkillTable Test Case
 */
class TechniqueSkillTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TechniqueSkillTable
     */
    public $TechniqueSkill;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.technique_skill'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TechniqueSkill') ? [] : ['className' => 'App\Model\Table\TechniqueSkillTable'];
        $this->TechniqueSkill = TableRegistry::get('TechniqueSkill', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TechniqueSkill);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
