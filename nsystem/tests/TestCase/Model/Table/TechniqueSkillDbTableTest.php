<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TechniqueSkillDbTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TechniqueSkillDbTable Test Case
 */
class TechniqueSkillDbTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TechniqueSkillDbTable
     */
    public $TechniqueSkillDb;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.technique_skill_db'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TechniqueSkillDb') ? [] : ['className' => 'App\Model\Table\TechniqueSkillDbTable'];
        $this->TechniqueSkillDb = TableRegistry::get('TechniqueSkillDb', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TechniqueSkillDb);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
