<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SalesSkillTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SalesSkillTable Test Case
 */
class SalesSkillTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SalesSkillTable
     */
    public $SalesSkill;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.sales_skill'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SalesSkill') ? [] : ['className' => 'App\Model\Table\SalesSkillTable'];
        $this->SalesSkill = TableRegistry::get('SalesSkill', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SalesSkill);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
